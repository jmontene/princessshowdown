﻿using UnityEngine;

namespace Tricolor.System
{
    /// <summary>
    /// Class implementing the singleton pattern
    /// </summary>
    public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        #region Static Members

        /// <summary>
        /// The singleton instance
        /// </summary>
        public static T Instance { get; private set; }

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected virtual void Awake()
        {
            if(Instance == null)
            {
                Instance = this as T;
            }
            else if(Instance != this)
            {
                DestroyImmediate(gameObject);
            }
        }

        #endregion
    }
}
