﻿using UnityEngine;

namespace Tricolor.Engine
{
    [CreateAssetMenu(fileName = "PlayerForm", menuName = "Tricolor/Actor/Form", order = 2)]
    public class PlayerForm : ScriptableObject
    {
        public int numberOfJumps;
        public float dashSpeed;
        public float dashDuration;
        public int airDashes;
        public bool canDash;
        public AudioClip switchVoice;
        public ActionBehaviourBase basicAttack;
    }
}
