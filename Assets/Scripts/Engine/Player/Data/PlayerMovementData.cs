﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data for player movement
    /// </summary>
    [Serializable]
    public class PlayerMovementData : IData
    {
        #region Public Members

        /// <summary>
        /// The initial speed of a jump
        /// </summary>
        public float JumpTakeOffSpeed = 7f;

        public float BounceTakeOffSpeed = 15f;

        public float DashSpeed = 7f;

        public float DashDuration = 0.1f;

        public int AerialJumps = 0;

        public bool DashEnabled = false;

        [HideInInspector]
        public int CurrentAerialJumps = 0;

        [HideInInspector]
        public int AirDashes = 0;
        [HideInInspector]
        public int CurrentAirDashes = 0;
        [HideInInspector]
        public bool airDashing = false; 

        #endregion

        #region IData Interface

        /// <summary>
        /// Initialize this data
        /// </summary>
        public void Init()
        {
            CurrentAerialJumps = AerialJumps;
        }

        #endregion

    }
}
