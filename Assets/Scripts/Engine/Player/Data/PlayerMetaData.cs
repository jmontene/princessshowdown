﻿using UnityEngine;
using Cinemachine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Container for all player data
    /// </summary>
    public class PlayerMetaData : ActorMetaData
    {
        #region Public Members

        public Material mainMaterial;

        [HideInInspector]
        public PlayerForm currentPlayerForm;

        [Header("Player")]

        /// <summary>
        /// Movement data
        /// </summary>
        public PlayerMovementData PlayerMovementData;

        public bool shieldEnabled = false;
        public bool switchEnabled = false;
        public bool canShield = true;

        public float shieldRefreshCooldown = 0.4f;

        public AudioClip[] jumpVoices;
        public AudioClip[] attackVoices;
        public AudioClip[] hurtVoices;
        public AudioClip[] reviveVoices;
        public AudioClip switchSound;
        public AudioClip switchVoice;
        public AudioClip airDashSound;

        public CinemachineVirtualCamera followCam;
        public CinemachineVirtualCamera currentCam;
        public Checkpoint currentCheckpoint;

        #endregion

        #region MetaDataBase Overrrides

        /// <summary>
        /// Initialize the data contained here
        /// </summary>
        protected override void InitializeData()
        {
            base.InitializeData();
            data.Add(PlayerMovementData);
        }

        #endregion
    }
}
