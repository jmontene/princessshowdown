﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Controller class for player
    /// </summary>
    public class PlayerController : ActorController
    {

        [HideInInspector]
        public GameObject currentShield;
        [HideInInspector]
        public PlayerMetaData playerMetaData;

        public PlayerForm waterForm;
        public PlayerForm windForm;
        public PlayerForm fireForm;
        public PlayerForm earthForm;
        public PlayerForm fakeEarthForm;

        #region Events

        /// <summary>
        /// Jump event
        /// </summary>
        public event EventHandler JumpEvent;

        /// <summary>
        /// Jump Interrupt Event
        /// </summary>
        public event EventHandler JumpInterruptEvent;

        /// <summary>
        /// Dash event
        /// </summary>
        public event EventHandler DashEvent;

        /// <summary>
        /// Shield event
        /// </summary>
        public event EventHandler ShieldEvent;

        /// <summary>
        /// Switch event
        /// </summary>
        public event EventHandler SwitchEvent;

        public event EventHandler ReviveEvent;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            playerMetaData = GetComponent<PlayerMetaData>();
        }

        protected override void Start()
        {
            base.Start();
            playerMetaData.followCam.MoveToTopOfPrioritySubqueue();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Fire Jump Event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event args</param>
        public void Jump(object _sender, EventArgs _emptyEventArgs)
        {
            JumpEvent?.Invoke(_sender, _emptyEventArgs);
        }

        /// <summary>
        /// Fire Interrupt Jump Event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event args</param>
        public void InterruptJump(object _sender, EventArgs _emptyEventArgs)
        {
            JumpInterruptEvent?.Invoke(_sender, _emptyEventArgs);
        }

        public void Dash(object _sender, EventArgs _emptyEventArgs)
        {
            DashEvent?.Invoke(_sender, _emptyEventArgs);
        }

        public void Shield(object _sender, EventArgs _emptyEventArgs)
        {
            ShieldEvent?.Invoke(_sender, _emptyEventArgs);
        }

        public void Switch(object _sender, EventArgs _emptyEventArgs)
        {
            SwitchEvent?.Invoke(_sender, _emptyEventArgs);
        }

        public void Revive(object _sender, EventArgs _emptyEventArgs)
        {
            ReviveEvent?.Invoke(_sender, _emptyEventArgs);
        }

        #endregion

        #region Interfacing Methods

        public void PlayAttackVoice()
        {
            var clips = playerMetaData.attackVoices;
            var index = UnityEngine.Random.Range(0, clips.Length);
            metaData.audioSource.PlayOneShot(clips[index]);
        }

        public void PlayHurtVoice()
        {
            var clips = playerMetaData.hurtVoices;
            var index = UnityEngine.Random.Range(0, clips.Length);
            metaData.audioSource.PlayOneShot(clips[index]);
        }

        public void PlayReviveVoice()
        {
            var clips = playerMetaData.reviveVoices;
            var index = UnityEngine.Random.Range(0, clips.Length);
            metaData.audioSource.PlayOneShot(clips[index]);
        }

        public void PlaySwitchSound()
        {
            metaData.audioSource.PlayOneShot(playerMetaData.switchSound, 2f);
        }

        public void PlayerDeathStun()
        {
            TimeManager.Instance.Slowdown(0f, 0.5f);
        }

        public void StartFollowShake()
        {
            playerMetaData.currentCam.GetComponent<CameraShake>().StartShake();
        }

        public void StopFollowShake()
        {
            playerMetaData.currentCam.GetComponent<CameraShake>().StopShake();
        }

        public override void UnlockActor()
        {
            base.UnlockActor();
            StopFollowShake();
        }

        #endregion

    }
}
