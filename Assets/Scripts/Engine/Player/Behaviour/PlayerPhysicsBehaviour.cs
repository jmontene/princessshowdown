﻿namespace Tricolor.Engine
{
    public class PlayerPhysicsBehaviour : ActorPhysicsBehaviour
    {
        #region Protected Members

        protected PlayerMovementData playerMovementData;

        #endregion

        protected override void Start()
        {
            base.Start();
            playerMovementData = GetComponent<PlayerMetaData>().PlayerMovementData;
        }

        protected override void OnGrounded()
        {
            base.OnGrounded();
            playerMovementData.CurrentAerialJumps = playerMovementData.AerialJumps;
            playerMovementData.CurrentAirDashes = playerMovementData.AirDashes;
        }
    }
}
