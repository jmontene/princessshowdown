﻿using UnityEngine;
using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Input listener for Player
    /// </summary>
    public class PlayerInputBehaviour : ActorInputBehaviour
    {

        #region Protected Members

        /// <summary>
        /// Reference to the player controller
        /// </summary>
        protected PlayerController playerController;

        #endregion

        #region Unity Methods

        protected override void Start()
        {
            base.Start();

            InputManager.Instance.SubscribeToPressedEvent(ButtonNames.JUMP, OnJumpPressed);
            InputManager.Instance.SubscribeToReleasedEvent(ButtonNames.JUMP, OnJumpReleased);
            InputManager.Instance.SubscribeToPressedEvent(ButtonNames.ATTACK, OnAttackPressed);
            InputManager.Instance.SubscribeToPressedEvent(ButtonNames.DASH, OnDashPressed);
            InputManager.Instance.SubscribeToPressedEvent(ButtonNames.SHIELD, OnShieldPressed);
            InputManager.Instance.SubscribeToPressedEvent(ButtonNames.SWITCH, OnSwitchPressed);
        }

        #endregion

        #region ActorInputBehaviour Overrides

        /// <summary>
        /// Process this frame's input
        /// </summary>
        protected override void ProcessInput()
        {
            base.ProcessInput();
            var moveData = metaData.GetData<ActorMovementData>();
            var physicsData = metaData.GetData<ActorPhysicsData>();

            moveData.SetDirection(new Vector2(directionalInput.x * moveData.DirectionScale.x, directionalInput.y * moveData.DirectionScale.y));

            if(directionalInput.x != 0f && Mathf.Sign(directionalInput.x) != Mathf.Sign(physicsData.FacingDirection.x))
            {
                physicsData.FacingDirection = new Vector2(Mathf.Sign(directionalInput.x), 1f);
                gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
            }
        }

        #endregion

        #region Event Handlers

        protected void OnJumpPressed(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.Jump(_sender, _args);
        }

        protected void OnJumpReleased(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.InterruptJump(_sender, _args);
        }

        protected void OnAttackPressed(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.Attack(_sender, _args);
        }

        protected void OnDashPressed(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.Dash(_sender, _args);
        }

        protected void OnShieldPressed(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.Shield(_sender, _args);
        }

        protected void OnSwitchPressed(object _sender, EventArgs _args)
        {
            if (!playerController.playerMetaData.InputData.ListenToInput) return;
            playerController.Switch(_sender, _args);
        }

        #endregion

        #region EntityBehaviour Overrides

        protected override void GetController()
        {
            base.GetController();
            playerController = GetComponent<PlayerController>();
        }

        #endregion
    }
}
