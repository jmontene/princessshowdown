﻿using UnityEngine;
using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Movement behaviour for player
    /// </summary>
    public class PlayerMovementBehaviour : ActorMovementBehaviour
    {
        #region Protected Members

        /// <summary>
        /// Reference to the player controller
        /// </summary>
        protected PlayerController playerController;

        /// <summary>
        /// Actor physics data
        /// </summary>
        protected ActorPhysicsData physicsData;

        protected ActorMovementData moveData;

        protected ActorInputData inputData;

        /// <summary>
        /// Player movement data
        /// </summary>
        protected PlayerMovementData playerMovementData;

        protected Timer dashTimer;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialization of components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Start()
        {
            base.Start();
            physicsData = metaData.GetData<ActorPhysicsData>();
            moveData = metaData.GetData<ActorMovementData>();
            inputData = metaData.GetData<ActorInputData>();
            playerMovementData = metaData.GetData<PlayerMovementData>();

            //Subscribe to events
            playerController.JumpEvent += HandleJump;
            playerController.JumpInterruptEvent += HandleJumpInterrupt;
            playerController.DashEvent += HandleDash;

            dashTimer = new Timer(StopDash, playerMovementData.DashDuration);
        }

        #endregion

        #region EntityBehaviour Overrides

        protected override void GetController()
        {
            base.GetController();
            playerController = GetComponent<PlayerController>();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Execute a Jump
        /// </summary>
        protected void Jump()
        {
            if (playerMovementData.airDashing) return;

            if (playerMovementData.CurrentAerialJumps < 1 && !physicsData.Grounded)
            {
                return;
            }

            if (!physicsData.Grounded)
            {
                playerMovementData.CurrentAerialJumps--;
            }

            Vector2 v = physicsData.Velocity;
            v.y = playerMovementData.JumpTakeOffSpeed;
            physicsData.Velocity = v;

            var voices = playerController.playerMetaData.jumpVoices;
            int rIndex = UnityEngine.Random.Range(0, voices.Length);
            AudioClip jumpVoice = voices[rIndex];
            playerController.playerMetaData.audioSource.PlayOneShot(jumpVoice);
        }

        protected void Dash()
        {
            if (!playerMovementData.DashEnabled) return;

            if (playerMovementData.CurrentAirDashes < 1 && !physicsData.Grounded)
            {
                return;
            }

            physicsData.xGlobalSpeed = playerMovementData.DashSpeed * Mathf.Sign(physicsData.FacingDirection.x);
            if (!physicsData.Grounded)
            {
                physicsData.GravityScale = 0f;
                playerMovementData.CurrentAirDashes--;

                Vector2 v = physicsData.Velocity;
                v.y = 0f;
                physicsData.Velocity = v;

                playerMovementData.airDashing = true;
            }

            if(playerController.playerMetaData.currentPlayerForm == playerController.waterForm)
            {
                actorController.SetAnimationTrigger("Dash");
            }
            else
            {
                AudioManager.Instance.PlaySFX(playerController.playerMetaData.airDashSound, 4f);
                actorController.SetAnimationTrigger("WindDash");
            }

            dashTimer.SetDuration(playerMovementData.DashDuration);
            dashTimer.Start();
        }

        protected void StopDash()
        {
            physicsData.xGlobalSpeed = 0f;
            physicsData.GravityScale = 1f;
            playerMovementData.airDashing = false;
        }

        /// <summary>
        /// Interrupt a midair jump
        /// </summary>
        protected void ReleaseJump()
        {
            Vector2 v = physicsData.Velocity;
            if(v.y > 0)
            {
                v.y *= 0.5f;
                physicsData.Velocity = v;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handle Jump Event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event arguments</param>
        protected void HandleJump(object _sender, EventArgs _emptyEventArgs)
        {
            Jump();
        }

        /// <summary>
        /// Handle Jump Interrupt Event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event arguments</param>
        protected void HandleJumpInterrupt(object _sender, EventArgs _emptyEventArgs)
        {
            ReleaseJump();
        }

        protected void HandleDash(object _sender, EventArgs _args)
        {
            Dash();
        }

        #endregion


    }
}
