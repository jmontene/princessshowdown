﻿using System;

namespace Tricolor.Engine
{
    public class PlayerStateBehaviour : ActorStateBehaviour
    {
        public bool gameOver = false;

        protected PlayerController playerController;
        protected PlayerMetaData playerMetaData;
        protected EventArgs eventArgs;

        protected override void Awake()
        {
            base.Awake();
            playerController = GetComponent<PlayerController>();
            playerMetaData = GetComponent<PlayerMetaData>();
            eventArgs = new EventArgs();
        }

        public void RefreshToWorld()
        {
            if (gameOver) return;
            Checkpoint checkpoint = playerMetaData.currentCheckpoint;
            checkpoint.vCam.MoveToTopOfPrioritySubqueue();
            playerMetaData.followCam.Follow = null;
            transform.position = checkpoint.transform.position;

            playerMetaData.ActorStats.ResetStat(ActorStrings.STAT_HP);
            PlayerUI.Instance.ResetHearts();
            actorController.metaData.isDead = false;
        }

        public void RefocusToPlayer()
        {
            actorController.UnlockActor();
            Checkpoint checkpoint = playerMetaData.currentCheckpoint;

            playerMetaData.followCam.Follow = transform;
            playerMetaData.followCam.MoveToTopOfPrioritySubqueue();

            playerController.Revive(this, eventArgs);
        }

        protected override void ReduceHP(int damage)
        {
            base.ReduceHP(damage);
            PlayerUI.Instance.DealDamage(damage);
        }
    }
    
}
