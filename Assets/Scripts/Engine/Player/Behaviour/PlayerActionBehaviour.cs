﻿using System;
using UnityEngine;
using Anima2D;
using Tricolor.Gameplay;

namespace Tricolor.Engine
{
    public class PlayerActionBehaviour : ActorActionBehaviour
    {
        #region Public Members

        public GameObject currentShield;
        public float shieldAliveTime;
        public SpriteMeshAnimation bodyAnim;
        public SpriteMeshAnimation chestAnim;

        #endregion

        #region Protected Members

        protected PlayerController playerController;

        protected Timer shieldTimer;
        protected Timer shieldRefreshTimer;

        protected Animator shieldAnimator;

        protected AudioClip attackSound;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            playerController = GetComponent<PlayerController>();
            shieldAnimator = currentShield.GetComponent<Animator>();
        }

        protected override void Start()
        {
            base.Start();
            playerController.ShieldEvent += HandleShield;
            playerController.CollideEvent += HandleCollide;

            shieldTimer = new Timer(RemoveShield, shieldAliveTime);
            shieldRefreshTimer = new Timer(RefreshShieldCooldown, playerController.playerMetaData.shieldRefreshCooldown);

            actorController.metaData.PhysicsData.ignoredColliders.Add(currentShield.GetComponent<Collider2D>());
            currentShield.SetActive(false);

            playerController.SwitchEvent += HandleSwitch;

            playerController.playerMetaData.currentPlayerForm = null;
        }

        #endregion

        #region Public Methods

        public void RemoveShield()
        {
            shieldAnimator.SetTrigger("Disappear");
            actorController.shielded = false;
        }

        public void DoFormChange()
        {
            PlayerMetaData d = playerController.playerMetaData;
            if(d.currentPlayerForm == playerController.fireForm)
            {
                SwitchToFireForm();
                d.switchVoice = playerController.fireForm.switchVoice;
            }else if(d.currentPlayerForm == playerController.earthForm)
            {
                SwitchToEarthForm();
                d.switchVoice = playerController.earthForm.switchVoice;
            }else if (d.currentPlayerForm == null)
            {
                d.currentPlayerForm = playerController.waterForm;
                d.switchVoice = playerController.waterForm.switchVoice;
                ApplyForm(d.currentPlayerForm);
            }else if (d.currentPlayerForm == playerController.fakeEarthForm)
            {
                SwitchToFireForm();
                d.switchVoice = playerController.fakeEarthForm.switchVoice;
            }
            else {
                d.currentPlayerForm = (d.currentPlayerForm == playerController.waterForm ? playerController.windForm : playerController.waterForm);
                ApplyForm(d.currentPlayerForm);
            }
            d.audioSource.PlayOneShot(d.switchVoice);
        }

        #endregion

        #region Protected Methods

        protected void Shield()
        {
            if (!playerController.playerMetaData.shieldEnabled) return;
            if (playerController.playerMetaData.currentPlayerForm != playerController.waterForm) return;
            if (!playerController.playerMetaData.canShield) return;
            currentShield.SetActive(true);
            actorController.SetAnimationTrigger("Shield");
            actorController.shielded = true;
            playerController.playerMetaData.canShield = false;
            shieldTimer.Start();
            shieldRefreshTimer.Start();
        }

        protected void RefreshShieldCooldown()
        {
            playerController.playerMetaData.canShield = true;
        }

        protected void Switch()
        {
            if (!playerController.playerMetaData.switchEnabled) return;
            actorController.SetAnimationTrigger("Switch");
        }

        protected void Collided(ActorController other)
        {
            if (other == null) return;
            if (other.CompareTag("Refresher")){
                PlayerMovementData m = playerController.playerMetaData.PlayerMovementData;
                if (!m.airDashing) return;

                m.CurrentAerialJumps = m.AerialJumps;
                m.CurrentAirDashes = m.AirDashes;
                other.GetComponent<Refresher>().Refreshed();
            }else if (other.CompareTag("Trigger"))
            {
                var trigger = other.GetComponent<TriggerCommandBase>();
                trigger.OnTriggerCollided();
            }else if (other.CompareTag("Bouncer") && playerController.shielded)
            {
                other.GetComponent<Bouncer>().Bounce();
                Bounce();
            }else if (other.CompareTag("MagicOrb"))
            {
                ScoringManager.Instance.AddMagicOrb();
                other.gameObject.SetActive(false);
            }
        }

        protected void Bounce()
        {
            Vector2 v = playerController.metaData.PhysicsData.Velocity;
            v.y = playerController.playerMetaData.PlayerMovementData.BounceTakeOffSpeed;
            playerController.metaData.PhysicsData.Velocity = v;
        }

        protected void ApplyForm(PlayerForm form)
        {
            PlayerMovementData m = playerController.playerMetaData.PlayerMovementData;
            playerController.playerMetaData.switchVoice = form.switchVoice;

            m.AerialJumps = form.numberOfJumps-1;
            m.CurrentAerialJumps = form.numberOfJumps - 1;
            m.DashSpeed = form.dashSpeed;
            m.DashDuration = form.dashDuration;
            m.AirDashes = form.airDashes;
            m.CurrentAirDashes = form.airDashes;
            m.DashEnabled = form.canDash;

            actionData.BasicAttack = form.basicAttack;
            InitBasicAttack();

            if(playerController.windForm == form)
            {
                bodyAnim.frame = 1;
                chestAnim.frame = 1;
            }else {
                bodyAnim.frame = 0;
                chestAnim.frame = 0;
            }
        }

        protected void SwitchToFireForm()
        {
            bodyAnim.frame = 2;
            chestAnim.frame = 2;
        }

        protected void SwitchToEarthForm()
        {
            bodyAnim.frame = 3;
            chestAnim.frame = 3;
        }

        #endregion

        #region Event Handlers

        protected void HandleShield(object _sender, EventArgs _args)
        {
            Shield();
        }

        protected void HandleSwitch(object _sender, EventArgs _args)
        {
            Switch();
        }

        protected void HandleCollide(object _sender, CollisionEventArgs _args)
        {
            Collided(_args.Other);
        }

        #endregion
    }
}
