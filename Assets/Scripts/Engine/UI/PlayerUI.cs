﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using Tricolor.System;

namespace Tricolor.Engine
{
    public class PlayerUI : SingletonBehaviour<PlayerUI>
    {
        public List<Image> hearts;
        public Color damageColor;

        protected int heartIndex;

        protected override void Awake()
        {
            base.Awake();
            heartIndex = hearts.Count - 1;
        }

        public void ResetHearts()
        {
            foreach(Image heart in hearts)
            {
                heart.color = Color.white;
            }
            heartIndex = hearts.Count - 1;
        }

        public void DealDamage(int amount)
        {
            while(amount > 0)
            {
                if (heartIndex < 0) break;
                hearts[heartIndex--].color = damageColor;
                amount--;
            }
        }
    }
}
