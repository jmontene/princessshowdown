﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace Tricolor.Engine
{
    public class TitleScreen : MonoBehaviour
    {
        public Image fader;
        public GameObject title;
        public GameObject credits;
        public Button backButton;
        public Button startButton;
        public Image LoadingImage;

        protected bool buttonPressed = false;

        protected void Awake()
        {
            Color targetColor = fader.color;
            targetColor.a = 0f;
            fader.DOColor(targetColor, 2f);
        }

        public void OnStartButtonPressed()
        {
            if (buttonPressed) return;
            StartCoroutine(TransitionToLevel());
        }

        public void OnExitButtonPressed()
        {
            Application.Quit();
        }

        public void OnCreditsButtonPressed()
        {
            title.SetActive(false);
            credits.SetActive(true);
            backButton.Select();
        }

        public void OnBackButtonPressed()
        {
            credits.SetActive(false);
            title.SetActive(true);
            startButton.Select();
        }

        protected IEnumerator TransitionToLevel()
        {
            buttonPressed = true;
            Color targetColor = fader.color;
            targetColor.a = 1f;
            fader.DOColor(targetColor, 2f);
            yield return new WaitForSeconds(2f);
            LoadingImage.gameObject.SetActive(true);
            SceneManager.LoadScene("Level");
        }
    }
}
