﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace Tricolor.Engine
{
    public class RoseTitle : MonoBehaviour
    {
        public Image overlay;
        public float switchTime = 5f;
        public float overlayTime = 2f;
        public Color activeColor;
        public Color inactiveColor;

        protected Animator anim;
        protected Timer blueTimer;
        protected Timer tealTimer;
        protected Timer pinkTimer;

        protected void Awake()
        {
            anim = GetComponent<Animator>();
        }

        protected void Start()
        {
            blueTimer = new Timer(SwitchToBlue, switchTime);
            tealTimer = new Timer(SwitchToTeal, switchTime);
            pinkTimer = new Timer(SwitchToPink, switchTime);

            blueTimer.Start();
        }

        protected void SwitchToBlue()
        {
            StartCoroutine(SwitchTo("Blue", tealTimer));
        }

        protected void SwitchToTeal()
        {
            StartCoroutine(SwitchTo("Teal", pinkTimer));
        }

        protected void SwitchToPink()
        {
            StartCoroutine(SwitchTo("Pink", blueTimer));
        }

        protected IEnumerator SwitchTo(string form, Timer nextTimer)
        {
            overlay.DOColor(activeColor, overlayTime);
            yield return new WaitForSeconds(overlayTime);
            anim.SetTrigger(form);
            overlay.DOColor(inactiveColor, overlayTime);
            yield return new WaitForSeconds(overlayTime);
            nextTimer.Start();
        }


    }
}
