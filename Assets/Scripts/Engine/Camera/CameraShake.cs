﻿using UnityEngine;
using Cinemachine;

namespace Tricolor.Engine
{
    public class CameraShake : MonoBehaviour
    {
        public float targetAmplitude;
        public float targetFrequency;
        protected CinemachineBasicMultiChannelPerlin noise;

        protected void Awake()
        {
            CinemachineVirtualCamera cam = GetComponent<CinemachineVirtualCamera>();
            noise = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        public void StartShake()
        {
            noise.m_AmplitudeGain = targetAmplitude;
            noise.m_FrequencyGain = targetFrequency;
        }

        public void StopShake()
        {
            noise.m_AmplitudeGain = 0f;
            noise.m_FrequencyGain = 0f;
        }
    }
}
