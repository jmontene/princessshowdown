﻿using UnityEngine;

namespace Tricolor.Engine
{
    public class ParallaxBehaviour : EntityBehaviourBase
    {
        public float backGroundSize;

        protected Transform cameraTransform;
        protected Transform[] layers;
        protected float viewZone = 10;
        protected int leftIndex;
        protected int rightIndex;

        protected override void Awake()
        {
            base.Awake();

            cameraTransform = Camera.main.transform;
            layers = new Transform[transform.childCount];
            for(int i = 0; i < transform.childCount; ++i)
            {
                layers[i] = transform.GetChild(i);
            }
            leftIndex = 0;
            rightIndex = layers.Length - 1;
        }

        public override void EntityUpdate()
        {
            base.EntityUpdate();
            if(cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
            {
                ScrollLeft();
            }
            if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
            {
                ScrollLeft();
            }
        }

        protected void ScrollLeft()
        {
            int lastRight = rightIndex;
            layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - backGroundSize);
            leftIndex = rightIndex;
            rightIndex--;
            if(rightIndex < 0)
            {
                rightIndex = layers.Length - 1;
            }
        }

        protected void ScrollRight()
        {
            int lastLeft = leftIndex;
            layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + backGroundSize);
            rightIndex = leftIndex;
            leftIndex++;
            if (leftIndex == layers.Length)
            {
                leftIndex = 0;
            }
        }
    }
}
