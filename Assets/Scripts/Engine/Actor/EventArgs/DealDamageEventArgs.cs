﻿using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Event arguments for
    /// Deal Damage event
    /// </summary>
    public class DealDamageEventArgs : EventArgs
    {
        #region Public Members

        /// <summary>
        /// The amount of damage to deal
        /// </summary>
        public float Damage;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_damage">Damage to deal</param>
        public DealDamageEventArgs(float _damage)
        {
            Damage = _damage;
        }

        #endregion
    }
}
