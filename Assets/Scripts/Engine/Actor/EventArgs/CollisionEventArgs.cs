﻿using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Event arguments for Collision event
    /// </summary>
    public class CollisionEventArgs : EventArgs
    {
        /// <summary>
        /// The actor we collided with
        /// </summary>
        public ActorController Other;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_other">The actor we collided with</param>
        public CollisionEventArgs(ActorController _other)
        {
            Other = _other;
        }
    }
}
