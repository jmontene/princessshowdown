﻿using UnityEngine;
using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data for player movement
    /// </summary>
    [Serializable]
    public class ActorMovementData : IData
    {
        #region Inspector Members

        /// <summary>
        /// Movement speed
        /// </summary>
        public float MovementSpeed;

        public float DashSpeed;

        /// <summary>
        /// The scale of usage for Direction
        /// </summary>
        public Vector2 DirectionScale;

        #endregion

        #region Public Accessors

        /// <summary>
        /// Movement direction
        /// </summary>
        public Vector2 Direction { get; protected set; }

        /// <summary>
        /// The direction of the entity taking into account
        /// the direction scales
        /// </summary>
        public Vector2 EffectiveDirection;//{ get; protected set; }

        #endregion

        #region IData Interface

        /// <summary>
        /// Initialize the data using the inspector values
        /// </summary>
        public void Init()
        {
            SetDirectionScale(DirectionScale);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Set the current direction
        /// </summary>
        /// <param name="_direction">The direction to set to</param>
        public void SetDirection(Vector2 _direction)
        {
            Direction = _direction;
            UpdateEffectiveDirection();
        }

        /// <summary>
        /// Set the new direction scale
        /// </summary>
        /// <param name="_scale">The new direction scale</param>
        public void SetDirectionScale(Vector2 _scale)
        {
            DirectionScale.Set(_scale.x, _scale.y);
            UpdateEffectiveDirection();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Update the effective direction based on current direction and scale
        /// </summary>
        protected void UpdateEffectiveDirection()
        {
            EffectiveDirection = new Vector2(Direction.x * DirectionScale.x, Direction.y * DirectionScale.y);
        }

        #endregion
    }
}
