﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Container for actor data and data transformation events
    /// </summary>
    public class ActorMetaData : MetaDataBase
    {
        #region Inspector Members

        [Header("Actor")]

        /// <summary>
        /// Movement data
        /// </summary>
        public ActorMovementData MovementData;

        /// <summary>
        /// Input data
        /// </summary>
        public ActorInputData InputData;

        /// <summary>
        /// Physics data
        /// </summary>
        public ActorPhysicsData PhysicsData;

        /// <summary>
        /// Action Data
        /// </summary>
        public ActorActionData ActionData;

        /// <summary>
        /// Animation Data
        /// </summary>
        public ActorAnimationData AnimationData;

        /// <summary>
        /// The actor's pluggable data
        /// </summary>
        public ActorSpec ActorSpec;

        /// <summary>
        /// The stats associated with this actor
        /// </summary>
        [HideInInspector]
        public EntityStats ActorStats;

        public AudioSource audioSource;
        public AudioClip[] deathSounds;

        public bool isDead;
        public bool canBasicAttack;
        public bool isInvincible;

        #endregion

        #region MetaDataBase Overrides

        /// <summary>
        /// Initialize the data contained here
        /// </summary>
        protected override void InitializeData()
        {
            data.Add(MovementData);
            data.Add(InputData);
            data.Add(PhysicsData);
            PhysicsData.animationData = AnimationData;
            data.Add(ActionData);
            data.Add(AnimationData);
            InitializeActorStats();
            isDead = false;
            canBasicAttack = true;
        }

        protected void InitializeActorStats() {
            ActorStats = new EntityStats();
            ActorStats.AddStat(new Stat(ActorStrings.STAT_HP, ActorSpec.HP));
            ActorStats.AddStat(new Stat(ActorStrings.STAT_MP, ActorSpec.MP));
            ActorStats.AddStat(new Stat(ActorStrings.STAT_ATTACK, ActorSpec.Attack));
            ActorStats.AddStat(new Stat(ActorStrings.STAT_DEFENSE, ActorSpec.Defense));
        }

        #endregion
    }
}
