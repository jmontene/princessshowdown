﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Pluggable actor data
    /// </summary>
    [CreateAssetMenu(fileName = "ActorSpec", menuName = "Tricolor/Actor/ActorSpec", order = 1)]
    public class ActorSpec : ScriptableObject
    {
        [Header("Base Stats")]

        public float HP;
        public float MP;
        public float Attack;
        public float Defense;
    }
}
