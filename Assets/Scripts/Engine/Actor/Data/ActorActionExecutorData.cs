﻿using UnityEngine;
using Tricolor.Gameplay;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data containing action executor info for actors
    /// </summary>
    public class ActorActionExecutorData : IData
    {
        /// <summary>
        /// Executor for basic attack
        /// </summary>
        public ActionExecutor BasicAttackExecutor;

        #region IData interface
        public void Init()
        {    
        }
        #endregion

        #region Public Members

        public void Init(ActorController controller, GameObject _obj)
        {
            BasicAttackExecutor = new ActionExecutor(controller, _obj);
            BasicAttackExecutor.projectileOrigin = controller.projectileOrigin;
            BasicAttackExecutor.enemyAi = controller.GetComponent<BasicEnemyAI>();
        }

        #endregion
    }
}
