﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data for an actor's animation
    /// </summary>
    [Serializable]
    public class ActorAnimationData : IData
    {
        /// <summary>
        /// The actor's animator controller
        /// </summary>
        public Animator animator;

        public void Init()
        {
        }
    }
}
