﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data class for actions of this actor
    /// </summary>
    [Serializable]
    public class ActorActionData : IData
    {
        #region Public Members

        /// <summary>
        /// This actor's basic attack
        /// </summary>
        public ActionBehaviourBase BasicAttack;

        public ActionBehaviourBase ShieldAction;

        #endregion

        #region IData Interface

        /// <summary>
        /// Initialize this data
        /// </summary>
        public void Init()
        {
        }

        #endregion

    }
}
