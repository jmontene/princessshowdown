﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tricolor.Engine
{

    public struct CollisionData
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;
    }

    /// <summary>
    /// Data for an actor's physics
    /// </summary>
    [Serializable]
    public class ActorPhysicsData : IData
    {
        #region Inspector Members

        /// <summary>
        /// The scale applied to gravity
        /// </summary>
        public float GravityScale = 1f;

        /// <summary>
        /// The minimum Y for a normal to be
        /// considered coming from a grounded position
        /// </summary>
        public float MinGroundNormalY = 0.65f;

        /// <summary>
        /// Determines if collisions
        /// stop this actor's movement
        /// </summary>
        public bool DetectPhysicalCollisions = true;

        /// <summary>
        /// Determines if a collision detection
        /// fires an event
        /// </summary>
        public bool TriggerCollisionEvents = false;

        public bool isEthereal = false;

        #endregion

        #region Public Accessors

        /// <summary>
        /// The current velocity of the actor
        /// </summary>
        public Vector2 Velocity { get; set; }

        [HideInInspector]
        public CollisionData collisionData;

        /// <summary>
        /// Determines if the actor is grounded
        /// </summary>
        public bool Grounded {
            get { return _grounded; }
            set {
                _grounded = value;
                if(animationData.animator != null)
                {
                    animationData.animator.SetBool("Grounded", value);
                }
            }
        }
        private bool _grounded;

        /// <summary>
        /// The desired velocity to use during this frame
        /// </summary>
        public Vector2 targetVelocity { get; set; }

        /// <summary>
        /// The direction this actor is currently facing at
        /// </summary>
        public Vector2 FacingDirection {
            get { return _facingDirection;  }
            set{
                _facingDirection = value;
                if(Mathf.Sign(xGlobalSpeed) != Mathf.Sign(_facingDirection.x))
                {
                    xGlobalSpeed = -xGlobalSpeed;
                }
            }
        }
        private Vector2 _facingDirection;

        /// <summary>
        /// The list of requested modifications to the targetVelocity
        /// </summary>
        public List<Vector2> targetVelocityModifierQueue { get; set; }

        /// <summary>
        /// Reference to the actor controller
        /// </summary>
        [HideInInspector]
        public ActorAnimationData animationData;

        [HideInInspector]
        public float xGlobalSpeed;

        /// <summary>
        /// Colliders to ignore in collision detection calculations
        /// </summary>
        [HideInInspector]
        public List<Collider2D> ignoredColliders;

        #endregion

        #region IData Interface

        /// <summary>
        /// Initialize this data
        /// </summary>
        public void Init()
        {
            Velocity = Vector2.zero;
            Grounded = false;
            targetVelocityModifierQueue = new List<Vector2>();
            FacingDirection = Vector2.right;
            xGlobalSpeed = 0f;
            ignoredColliders = new List<Collider2D>();

            collisionData.down = false;
            collisionData.up = false;
            collisionData.left = false;
            collisionData.right = false;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add a request to modify the target velocity
        /// </summary>
        /// <param name="_velocity">The modification to make</param>
        public void ModifyTargetVelocity(Vector2 _velocity)
        {
            targetVelocityModifierQueue.Add(_velocity);
        }

        #endregion
    }
}
