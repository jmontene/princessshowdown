﻿using UnityEngine;
using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data for input behaviour for player
    /// </summary>
    [Serializable]
    public class ActorInputData : IData
    {
        #region Public Members

        /// <summary>
        /// Determines if the behaviour should listen to input
        /// </summary>
        public bool ListenToInput;

        #endregion

        #region IData Interface

        /// <summary>
        /// Initialize this data
        /// </summary>
        public void Init()
        {
        }

        #endregion
    }
}
