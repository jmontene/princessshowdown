﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Controller class for all actors
    /// </summary>
    public class ActorController : EntityControllerBase
    {
        #region Events

        /// <summary>
        /// Die event
        /// </summary>
        public event EventHandler DieEvent;

        /// <summary>
        /// Attack event
        /// </summary>
        public event EventHandler AttackEvent;

        /// <summary>
        /// Deal Damage event
        /// </summary>
        public event EventHandler<DealDamageEventArgs> DealDamageEvent;

        /// <summary>
        /// Collide event
        /// </summary>
        public event EventHandler<CollisionEventArgs> CollideEvent;

        #endregion

        #region Public Members

        /// <summary>
        /// Reference to the actor's metadata
        /// </summary>
        [HideInInspector]
        public ActorMetaData metaData;

        public bool shielded;
        public Transform projectileOrigin;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            metaData = GetComponent<ActorMetaData>();
            shielded = false;
        }

        #endregion

        #region Event Methods

        /// <summary>
        /// Die Attack event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">emptyEventArgs</param>
        public void Die(object _sender, EventArgs _emptyEventArgs)
        {
            DieEvent?.Invoke(_sender, _emptyEventArgs);
        }

        /// <summary>
        /// Fire Attack event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">emptyEventArgs</param>
        public void Attack(object _sender, EventArgs _emptyEventArgs)
        {
            AttackEvent?.Invoke(_sender, _emptyEventArgs);
        }

        /// <summary>
        /// Fire Deal Damage event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_args">Event arguments</param>
        public void DealDamage(object _sender, DealDamageEventArgs _args)
        {
            DealDamageEvent?.Invoke(_sender, _args);
        }

        /// <summary>
        /// Fire Collide event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_args">Event arguments</param>
        public void Collide(object _sender, CollisionEventArgs _args)
        {
            CollideEvent?.Invoke(_sender, _args);
        }

        #endregion

        #region Interfacing Methods

        /// <summary>
        /// Get a specific stat
        /// </summary>
        /// <param name="key">The stat key</param>
        /// <returns>The stat's value</returns>
        public float GetStat(string key)
        {
            return metaData.ActorStats.Get(key);
        }

        public float GetBaseStat(string key)
        {
            return metaData.ActorStats.GetBase(key);
        }

        /// <summary>
        /// Set this object's direction
        /// </summary>
        /// <param name="direction">The direction to set to</param>
        public void SetDirection(Vector2 direction)
        {
            metaData.MovementData.SetDirection(direction);
        }

        /// <summary>
        /// Set a float value in the controller's animator
        /// </summary>
        /// <param name="animKey">The key to set</param>
        /// <param name="value">The value to set the key to</param>
        public void SetAnimationFloat(string animKey, float value)
        {
            Animator anim = metaData.AnimationData.animator;
            if (anim == null) return;
            metaData.AnimationData.animator.SetFloat(animKey, value);
        }

        /// <summary>
        /// Set a trigger value in the controller's animator
        /// </summary>
        /// <param name="animKey">The key to set</param>
        public void SetAnimationTrigger(string animKey)
        {
            Animator anim = metaData.AnimationData.animator;
            if (anim == null) return;
            metaData.AnimationData.animator.SetTrigger(animKey);
        }

        /// <summary>
        /// Set a bool value in the controller's animator
        /// </summary>
        /// <param name="animKey">The key to set</param>
        /// <param name="value">The value to set the key to</param>
        public void SetAnimationBoolean(string animKey, bool value)
        {
            Animator anim = metaData.AnimationData.animator;
            if (anim == null) return;
            metaData.AnimationData.animator.SetBool(animKey, value);
        }

        /// <summary>
        /// Locks actor movement
        /// and stops aerial momentum
        /// </summary>
        public void LockActor()
        {
            metaData.InputData.ListenToInput = false;
            metaData.MovementData.SetDirection(Vector2.zero);
            metaData.PhysicsData.Velocity = Vector2.zero;
            metaData.PhysicsData.GravityScale = 0f;
        }

        /// <summary>
        /// Unlocks actor movement
        /// and stops aerial momentum
        /// </summary>
        public virtual void UnlockActor()
        {
            metaData.InputData.ListenToInput = true;
            metaData.PhysicsData.GravityScale = 1f;
        }

        public void HitStun()
        {
            TimeManager.Instance.Slowdown(0f, 0.1f);
        }

        public void OrderFlash()
        {
            EffectsManager.Instance.Flash();
        }

        public void PlayDeathSound()
        {
            if (metaData.audioSource == null || metaData.deathSounds.Length < 1) return;
            var clips = metaData.deathSounds;
            var index = UnityEngine.Random.Range(0, clips.Length);
            metaData.audioSource.PlayOneShot(clips[index]);
        }

        public void GiveInvincibility()
        {
            metaData.isInvincible = true;
        }

        public void RemoveInvincibility()
        {
            metaData.isInvincible = false;
        }

        #endregion
    }
}
