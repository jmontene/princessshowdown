﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tricolor.Engine
{

    /// <summary>
    /// Behaviour class for player movement
    /// </summary>
    public class ActorMovementBehaviour : EntityBehaviourBase
    {

        #region Protected Members

        /// <summary>
        /// Reference to the actor controller
        /// </summary>
        protected ActorController actorController;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            actorController = GetComponent<ActorController>();
        }

        #endregion

        #region Behaviour Overrides

        /// <summary>
        /// Update step
        /// </summary>
        public override void EntityUpdate()
        {
            ApplyMovement();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Apply values to velocity based on direction
        /// </summary>
        protected void ApplyMovement()
        {
            var moveData = metaData.GetData<ActorMovementData>();
            var physicsData = metaData.GetData<ActorPhysicsData>();

            physicsData.ModifyTargetVelocity(moveData.EffectiveDirection * moveData.MovementSpeed);
        }

        #endregion
    }

}
