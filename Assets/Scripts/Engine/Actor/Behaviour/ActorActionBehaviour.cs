﻿using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Behaviour that handles attacks and special actions
    /// for the actor
    /// </summary>
    public class ActorActionBehaviour : EntityBehaviourBase
    {

        public float basicAttackCooldown = 1f;
        public bool doAttackAsAnimation = true;

        #region Protected Members

        /// <summary>
        /// Reference to the actor controller
        /// </summary>
        protected ActorController actorController;

        /// <summary>
        /// Reference to the action data
        /// </summary>
        protected ActorActionData actionData;

        /// <summary>
        /// The action executor for the basic attack
        /// </summary>
        protected ActorActionExecutorData actionExecutorData;

        /// <summary>
        /// Timer for basic attack reset
        /// </summary>
        protected Timer basicAttackTimer;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// that depend on external references
        /// </summary>
        protected override void Start()
        {
            base.Start();

            actionData = metaData.GetData<ActorActionData>();
            actionExecutorData = new ActorActionExecutorData();
            actorController.AttackEvent += HandleAttack;

            basicAttackTimer = new Timer(ResetBasicAttack, basicAttackCooldown);

            //Initialize actions
            actionExecutorData.Init(actorController, gameObject);
            InitBasicAttack();
        }

        #endregion

        #region Protected Methods

        protected void InitBasicAttack()
        {
            actionData.BasicAttack?.Init(actionExecutorData.BasicAttackExecutor);
        }

        /// <summary>
        /// Execute the basic attack
        /// </summary>
        protected void DoBasicAttack()
        {   
            actionData.BasicAttack?.Execute(actionExecutorData.BasicAttackExecutor);
        }

        protected virtual void DoBasicAttackAnimation()
        {
            if (!actorController.metaData.canBasicAttack) return;
            if (actorController.metaData.isDead) return;
            actorController.metaData.canBasicAttack = false;
            basicAttackTimer.Start();

            if (doAttackAsAnimation)
            {
                actorController.SetAnimationTrigger("Attack");
            }
            else
            {
                DoBasicAttack();
            }
            
        }

        protected void ResetBasicAttack()
        {
            actorController.metaData.canBasicAttack = true;
        }

        #endregion

        #region EntityBehaviour Overrides

        protected override void GetController()
        {
            base.GetController();
            actorController = GetComponent<ActorController>();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handle Attack event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event arguments</param>
        protected void HandleAttack(object _sender, EventArgs _emptyEventArgs)
        {
            DoBasicAttackAnimation();
        }

        #endregion
    }
}
