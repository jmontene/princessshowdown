﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Input listener for actor
    /// </summary>
    public class ActorInputBehaviour : EntityBehaviourBase
    {
        #region Protected Members

        /// <summary>
        /// Current value of the directional input
        /// </summary>
        protected Vector2 directionalInput;

        /// <summary>
        /// Reference to the input data
        /// </summary>
        protected ActorInputData inputData;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            directionalInput = Vector2.zero;
        }

        /// <summary>
        /// Initialization that depends on external components
        /// </summary>
        protected override void Start()
        {
            base.Start();
            inputData = metaData.GetData<ActorInputData>();
        }

        #endregion

        #region EntityBehaviour Overrides

        /// <summary>
        /// Update behaviour each frame
        /// </summary>
        public override void EntityUpdate()
        {
            ConsumeInput();

            if (!inputData.ListenToInput)
            {
                return;
            }

            ProcessInput();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Consume this frame's input
        /// </summary>
        protected void ConsumeInput()
        {
            directionalInput.Set(InputManager.Instance.GetAxis(AxisStrings.HORIZONTAL), InputManager.Instance.GetAxis(AxisStrings.VERTICAL));
        }

        /// <summary>
        /// Process this frame's input
        /// </summary>
        protected virtual void ProcessInput()
        {
        }

        #endregion
    }
}
