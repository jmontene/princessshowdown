﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Behaviour class that manages actor state
    /// </summary>
    public class ActorStateBehaviour : EntityBehaviourBase
    {
        #region Protected Members

        /// <summary>
        /// Reference to the actor metaData
        /// </summary>
        protected ActorMetaData actorMetaData;

        /// <summary>
        /// Reference to the actor controller
        /// </summary>
        protected ActorController actorController;

        protected EventArgs eventArgs;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            actorMetaData = GetComponent<ActorMetaData>();
            actorController = GetComponent<ActorController>();

            actorController.DealDamageEvent += HandleDealDamage;
            eventArgs = new EventArgs();
        }

        #endregion

        #region Protected Members

        /// <summary>
        /// Deal damage to this actor
        /// </summary>
        /// <param name="damage">The damage to deal</param>
        protected void DealDamage(float damage)
        {
            if (actorMetaData.isDead || actorMetaData.isInvincible) return;
            float totalDamage = damage - actorMetaData.ActorStats.Get(ActorStrings.STAT_DEFENSE);

            ReduceHP((int)totalDamage);
            float HP = actorMetaData.ActorStats.Get(ActorStrings.STAT_HP);
            if(HP > 0)
            {
                actorController.SetAnimationTrigger("Hurt");
            }
            else
            {
                actorController.SetAnimationTrigger("Death");
                actorMetaData.isDead = true;
                actorController.Die(this, eventArgs);
            }
            
        }

        protected virtual void ReduceHP(int damage)
        {
            actorMetaData.ActorStats.ModifyStat(ActorStrings.STAT_HP, -damage);
        }

        public void RemoveFromWorld()
        {
            gameObject.SetActive(false);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handle deal damage event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_args">Event arguments</param>
        protected void HandleDealDamage(object _sender, DealDamageEventArgs _args)
        {
            DealDamage(_args.Damage);
        }

        #endregion
    }
}
