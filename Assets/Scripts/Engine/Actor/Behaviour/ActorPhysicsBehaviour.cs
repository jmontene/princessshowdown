﻿using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Behaviour class for actor physics
    /// </summary>
    public class ActorPhysicsBehaviour : EntityBehaviourBase
    {
        #region Constants

        /// <summary>
        /// Minimum distance the actor needs to move before checking for collisions
        /// </summary>
        protected const float minMoveDistance = 0.001f;

        /// <summary>
        /// A small amount of padding to make sure
        /// we don't get stuck inside another collider
        /// </summary>
        protected const float shellRadius = 0.05f;

        #endregion

        #region Protected Members

        /// <summary>
        /// Reference to this object's rigidbody
        /// </summary>
        protected Rigidbody2D rb2d;

        /// <summary>
        /// Reference to the physicsData
        /// </summary>
        protected ActorPhysicsData physicsData;

        /// <summary>
        /// Filter for collision
        /// </summary>
        protected ContactFilter2D contactFilter;

        /// <summary>
        /// The buffer for storing raycast results
        /// during collision detection
        /// </summary>
        protected RaycastHit2D[] hitBuffer;

        protected Collider2D[] attachedColliders;

        /// <summary>
        /// The normal that is considered ground
        /// at the moment
        /// </summary>
        protected Vector2 groundNormal;

        /// <summary>
        /// Reference to the actor controller;
        /// </summary>
        protected ActorController actorController;

        #endregion

        #region Reusable Event Args

        /// <summary>
        /// Collision event args
        /// </summary>
        protected CollisionEventArgs collisionEventArgs;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            rb2d = GetComponent<Rigidbody2D>();

            contactFilter.useTriggers = false;
            contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            contactFilter.useLayerMask = true;

            hitBuffer = new RaycastHit2D[16];
            attachedColliders = new Collider2D[rb2d.attachedColliderCount];

            groundNormal = Vector2.up;

            actorController = GetComponent<ActorController>();
            collisionEventArgs = new CollisionEventArgs(null);
        }

        /// <summary>
        /// Initialize components that depend on external
        /// factors
        /// </summary>
        protected override void Start()
        {
            base.Start();
            physicsData = metaData.GetData<ActorPhysicsData>();
        }

        #endregion

        #region IEntity Interface

        /// <summary>
        /// Update step
        /// </summary>
        public override void EntityUpdate()
        {
            physicsData.targetVelocity = Vector2.zero;
            ComputeTargetVelocity();
            actorController.SetAnimationFloat("VelocityX", Mathf.Abs(physicsData.Velocity.x));
            actorController.SetAnimationFloat("VelocityY", physicsData.Velocity.y);
        }

        /// <summary>
        /// FixedUpdate step
        /// </summary>
        public void FixedUpdate()
        {
            var v = physicsData.Velocity;

            if (!physicsData.isEthereal)
            {
                v += physicsData.GravityScale * Physics2D.gravity * Time.fixedDeltaTime;
            }
            else
            {
                v.y = physicsData.targetVelocity.y;
            }
            
            v.x = physicsData.targetVelocity.x + physicsData.xGlobalSpeed;
            physicsData.Velocity = v;
            physicsData.Grounded = false;

            Vector2 deltaPosition = physicsData.Velocity * Time.fixedDeltaTime;

            Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);
            Vector2 xMove = moveAlongGround * deltaPosition.x;
            Move(xMove, false);

            Vector2 yMove = Vector2.up * deltaPosition.y;
            Move(yMove, true);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Compute the target velocity
        /// </summary>
        protected void ComputeTargetVelocity()
        {
            foreach(Vector2 v in physicsData.targetVelocityModifierQueue)
            {
                physicsData.targetVelocity += v;
            }

            physicsData.targetVelocityModifierQueue.Clear();
        }

        /// <summary>
        /// Move this actor
        /// </summary>
        /// <param name="move">The movement we wish to make</param>
        /// <param name="yMovement">Determines if we are checking for vertical or horizontal movement</param>
        protected void Move(Vector2 move, bool yMovement)
        {
            float distance = move.magnitude;

            if(distance <= minMoveDistance)
            {
                return;
            }
            
            int count = rb2d.GetAttachedColliders(attachedColliders);

            for(int i=0; i<count; ++i)
            {
                if (physicsData.ignoredColliders.Contains(attachedColliders[i]))
                {
                    continue;
                }
                int cols = attachedColliders[i].Cast(move, contactFilter, hitBuffer, distance + shellRadius);

                for (int j = 0; j < cols; ++j)
                {
                    if (physicsData.DetectPhysicalCollisions)
                    {
                        MovementCollision(hitBuffer[j], yMovement, ref distance);
                    }

                    if (physicsData.TriggerCollisionEvents)
                    {
                        collisionEventArgs.Other = hitBuffer[j].transform.GetComponent<ActorController>();
                        actorController.Collide(this, collisionEventArgs);
                    }
                }
            }

            rb2d.position += move.normalized * distance * Time.timeScale;
        }

        protected void MovementCollision(RaycastHit2D hit, bool yMovement, ref float distance)
        {
            var controller = hit.transform.GetComponent<ActorController>();
            if (controller != null && !controller.metaData.PhysicsData.DetectPhysicalCollisions)
            {
                return;
            }
            Vector2 currentNormal = hit.normal;
            if (currentNormal.y > physicsData.MinGroundNormalY)
            {
                physicsData.Grounded = true;
                OnGrounded();
                if (yMovement)
                {
                    groundNormal = currentNormal;
                    currentNormal.x = 0;
                }
            }

            float projection = Vector2.Dot(physicsData.Velocity, currentNormal);
            if (projection < 0)
            {
                physicsData.Velocity -= projection * currentNormal;
            }

            float modifiedDistance = hit.distance - shellRadius;
            distance = modifiedDistance < distance ? modifiedDistance : distance;
        }

        protected virtual void OnGrounded()
        {

        }

        #endregion
    }
}
