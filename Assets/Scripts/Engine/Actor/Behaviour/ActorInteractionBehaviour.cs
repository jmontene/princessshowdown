﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Behaviour that manages interactions
    /// between actors, like collisions
    /// </summary>
    public class ActorInteractionBehaviour : EntityBehaviourBase
    {
        #region Protected Members

        protected ActorController actorController;
        protected DealDamageEventArgs dealDamageEventArgs;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            actorController = GetComponent<ActorController>();
            dealDamageEventArgs = new DealDamageEventArgs(0f);
        }

        protected override void Start()
        {
            base.Start();
            actorController.CollideEvent += HandleCollide;
        }

        #endregion

        #region Protected Methods

        protected virtual void ProcessCollision(ActorController other)
        {
            if (other == null) return;
            if (other.gameObject.CompareTag("Spikes"))
            {
                dealDamageEventArgs.Damage = 9000f;
                actorController.DealDamage(this, dealDamageEventArgs);
            }
        }

        #endregion

        #region Event Handlers

        protected void HandleCollide(object _sender, CollisionEventArgs _args)
        {
            ProcessCollision(_args.Other);
        }

        #endregion
    }
}
