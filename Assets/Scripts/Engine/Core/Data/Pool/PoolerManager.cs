﻿using Tricolor.System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Class that manages all the
    /// object pools
    /// </summary>
    public class PoolerManager : SingletonBehaviour<PoolerManager>
    {
        /// <summary>
        /// The pool for projectiles
        /// </summary>
        public GameObjectPooler projectilePooler;

        /// <summary>
        /// The pool for ice shards
        /// </summary>
        public GameObjectPooler iceShardPooler;

        public GameObjectPooler windVortexPooler;
    }
}
