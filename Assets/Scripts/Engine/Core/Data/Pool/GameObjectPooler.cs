﻿using Tricolor.System;
using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Manager for pooling copies of 
    /// a specific prefab
    /// </summary>
    public class GameObjectPooler : MonoBehaviour
    {
        #region Public Members

        /// <summary>
        /// The prefab to pool
        /// </summary>
        public GameObject objectToPool;

        /// <summary>
        /// The amount of copies to make
        /// </summary>
        public int amountToPool;

        #endregion

        #region Protected Members

        /// <summary>
        /// The list of objects in the pool
        /// </summary>
        protected List<GameObject> pool;

        #endregion


        #region Unity Methods

        /// <summary>
        /// Initialize references and components
        /// </summary>
        protected void Awake()
        {
            pool = new List<GameObject>();

            for(int i = 0; i < amountToPool; ++i)
            {
                GameObject cur = Instantiate<GameObject>(objectToPool);
                cur.SetActive(false);
                cur.transform.parent = transform;
                pool.Add(cur);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get a gameobject from the pool
        /// </summary>
        /// <returns>The desired gameObject</returns>
        public GameObject GetObject()
        {
            foreach(GameObject obj in pool)
            {
                if (!obj.activeInHierarchy)
                {
                    return obj;
                }
            }
            return null;
        }

        #endregion
    }
}
