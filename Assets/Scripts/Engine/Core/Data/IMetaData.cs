﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all metadata types
    /// </summary>
    public interface IMetaData
    {
        /// <summary>
        /// Get a specific type of data this metaData is holding
        /// </summary>
        /// <typeparam name="T">The type of data we are searching for</typeparam>
        /// <returns>The data we need</returns>
        T GetData<T>() where T : IData;
    }
}
