﻿namespace Tricolor.Engine
{
    /// <summary>
    /// A collection of strings
    /// for key concepts on actors
    /// </summary>
    public class ActorStrings
    {
        public static readonly string STAT_HP = "HP";
        public static readonly string STAT_MP = "MP";
        public static readonly string STAT_ATTACK = "Attack";
        public static readonly string STAT_DEFENSE = "Defense";
    }
}
