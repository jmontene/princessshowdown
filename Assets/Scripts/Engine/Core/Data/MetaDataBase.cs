﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Tricolor.Engine
{
    public class MetaDataBase : MonoBehaviour, IMetaData
    {

        #region Protected Members

        /// <summary>
        /// The datas contained in this metaData
        /// </summary>
        protected List<IData> data;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialization of components and references
        /// </summary>
        protected virtual void Awake()
        {
            data = new List<IData>();
            InitializeData();

            foreach(IData d in data)
            {
                d.Init();
            }
        }

        #endregion

        #region IMetaData Interface

        /// <summary>
        /// Get a specific type of data this metaData is holding
        /// </summary>
        /// <typeparam name="T">The type of data we are searching for</typeparam>
        /// <returns>The data we need</returns>
        public T GetData<T>() where T : IData
        {
            return (T)data.First(x => x is T);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize the data contained here
        /// </summary>
        protected virtual void InitializeData()
        {
        }

        #endregion
    }
}
