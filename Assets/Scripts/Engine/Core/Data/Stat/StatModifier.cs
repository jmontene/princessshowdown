﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Modifier for a single stat
    /// </summary>
    public class StatModifier : IStatModifier
    {
        #region Public Members

        /// <summary>
        /// The type of modifier
        /// </summary>
        public StatModifierType ModType;

        /// <summary>
        /// The value of the modifier
        /// </summary>
        public float Value;

        /// <summary>
        /// The name of the stat we are modifying
        /// </summary>
        public string StatName;

        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="_type">Modifier type</param>
        /// <param name="_value">Modifier value</param>
        public StatModifier(string _name, StatModifierType _type, float _value)
        {
            StatName = _name;
            ModType = _type;
            Value = _value;
        }

        #endregion

        #region IStatModifier

        /// <summary>
        /// Returns the modifier type
        /// </summary>
        /// <returns>The modifier type</returns>
        public StatModifierType GetModifierType()
        {
            return ModType;
        }

        /// <summary>
        /// Get the name of the modified stat
        /// </summary>
        /// <returns></returns>
        public string GetModifiedStat()
        {
            return StatName;
        }

        /// <summary>
        /// Get the value of the modifier
        /// </summary>
        /// <returns>The value of the modifier</returns>
        public float GetValue()
        {
            return Value;
        }

        #endregion
    }
}
