﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all stat classes
    /// </summary>
    public interface IStat
    {
        /// <summary>
        /// Return this stat's name
        /// </summary>
        /// <returns>The stat's name</returns>
        string GetName();

        /// <summary>
        /// Return base value
        /// </summary>
        /// <returns>Base value of the stat</returns>
        float GetBase();

        /// <summary>
        /// Return current value
        /// </summary>
        /// <returns>The current value of the stat</returns>
        float GetCurrent();

        /// <summary>
        /// Set the base value of a stat
        /// </summary>
        /// <param name="_val">The new base value</param>
        void SetBase(float _value);
    }
}
