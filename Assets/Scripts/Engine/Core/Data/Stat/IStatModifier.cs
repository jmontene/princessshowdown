﻿namespace Tricolor.Engine
{

    /// <summary>
    /// Enumeration to determine
    /// the type of a modifier
    /// </summary>
    public enum StatModifierType
    {
        Add,
        Multiply
    }

    /// <summary>
    /// Common signature for all stat modifier
    /// classes
    /// </summary>
    public interface IStatModifier
    {
        /// <summary>
        /// Get the type of modifier
        /// </summary>
        /// <returns>The modifier type</returns>
        StatModifierType GetModifierType();

        /// <summary>
        /// Get the name of the modified stat
        /// </summary>
        /// <returns>The name of the modified stat</returns>
        string GetModifiedStat();

        /// <summary>
        /// Get the value of the modifier
        /// </summary>
        /// <returns>The value of the modifier</returns>
        float GetValue();
    }
}
