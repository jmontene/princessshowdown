﻿using System.Collections.Generic;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Collection of an entity's stats
    /// </summary>
    public class EntityStats
    {
        #region Protected Members

        /// <summary>
        /// The entity's stats
        /// </summary>
        protected Dictionary<string, Stat> stats;

        /// <summary>
        /// The stat's applied modspecs
        /// </summary>
        protected Dictionary<string, ModifierSpec> modifiers;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public EntityStats()
        {
            stats = new Dictionary<string, Stat>();
            modifiers = new Dictionary<string, ModifierSpec>();
        }

        #endregion

        #region Public Members

        /// <summary>
        /// Add a stat
        /// </summary>
        /// <param name="stat">The stat to add</param>
        public void AddStat(Stat stat)
        {
            stats.Add(stat.GetName(), stat);
        }

        public void ResetStat(string _statName)
        {
            Stat stat = stats[_statName];
            stat.CurrentValue = stat.BaseValue;
        }

        /// <summary>
        /// Set the base value of a stat
        /// </summary>
        /// <param name="_statName">The stat to change</param>
        /// <param name="value">The new base value</param>
        public void SetStat(string _statName, float value)
        {
            stats[_statName].SetBase(value);
        }

        /// <summary>
        /// Modify the stat by the given value (additively)
        /// </summary>
        /// <param name="_statName">The stat to modify</param>
        /// <param name="value">The value to add</param>
        public void ModifyStat(string _statName, float value)
        {
            stats[_statName].CurrentValue += value;
        }

        /// <summary>
        /// Add a modifier spec
        /// </summary>
        /// <param name="spec">The spec to add</param>
        public void AddModifier(ModifierSpec spec)
        {
            modifiers.Add(spec.Name, spec);
        }

        /// <summary>
        /// Remove a spec
        /// </summary>
        /// <param name="modName">Name of the spec to remove</param>
        public void RemoveModifier(string modName)
        {
            modifiers.Remove(modName);
        }

        /// <summary>
        /// Get the base value of a stat
        /// </summary>
        /// <param name="_statName">The stat's name</param>
        /// <returns>The base value of the stat</returns>
        public float GetBase(string _statName)
        {
            Stat stat;
            stats.TryGetValue(_statName, out stat);

            return stat == null ? 0f : stat.BaseValue;
        }

        /// <summary>
        /// Get the final value of a stat
        /// counting modifiers
        /// </summary>
        /// <param name="_statName">The stat to get</param>
        /// <returns>Final value of the stat</returns>
        public float Get(string _statName)
        {
            Stat stat;
            stats.TryGetValue(_statName, out stat);

            if(stat == null)
            {
                return 0;
            }

            float total = stat.CurrentValue;
            float multiplier = 1f;

            foreach(ModifierSpec spec in modifiers.Values)
            {
                foreach(IStatModifier mod in spec.GetModifiersForStat(stat.GetName(), StatModifierType.Add))
                {
                    total += mod.GetValue();
                }

                foreach (IStatModifier mod in spec.GetModifiersForStat(stat.GetName(), StatModifierType.Multiply))
                {
                    multiplier += mod.GetValue();
                }
            }

            return total * multiplier;
        }

        #endregion
    }
}
