﻿using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// A collection of modifiers
    /// packed under a name
    /// </summary>
    public class ModifierSpec
    {
        #region Public Members

        /// <summary>
        /// Name of the spec
        /// </summary>
        public string Name { get; protected set; }

        #endregion

        #region Protected Members

        /// <summary>
        /// Dictionary with the modifiers 
        /// arranged by type
        /// </summary>
        protected Dictionary<StatModifierType, Dictionary<string, List<IStatModifier>>> modifiers;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_name">Name of the spec</param>
        public ModifierSpec(string _name)
        {
            Name = _name;
            modifiers = new Dictionary<StatModifierType, Dictionary<string, List<IStatModifier>>>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add a modifier to the spec
        /// </summary>
        /// <param name="modifier">The modifier to add</param>
        public void AddModifier(IStatModifier modifier)
        {
            StatModifierType type = modifier.GetModifierType();

            if (!modifiers.ContainsKey(type))
            {
                modifiers[type] = new Dictionary<string, List<IStatModifier>>();
            }

            if (!modifiers[type].ContainsKey(modifier.GetModifiedStat()))
            {
                modifiers[type].Add(modifier.GetModifiedStat(), new List<IStatModifier>());
            }

            modifiers[type][modifier.GetModifiedStat()].Add(modifier);
        }

        /// <summary>
        /// Get additive modifiers for a single stat
        /// </summary>
        /// <param name="_statName">Name of the stat</param>
        /// <returns></returns>
        public List<IStatModifier> GetModifiersForStat(string _statName, StatModifierType _type)
        {
            List<IStatModifier> res = new List<IStatModifier>();

            if (modifiers.ContainsKey(_type))
            {
                return modifiers[_type][_statName];
            }

            return res;
        }

        #endregion

    }
}
