﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Data class for a single stat
    /// </summary>
    public class Stat
    {
        #region Public Members
        
        /// <summary>
        /// The name of the stat
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The base value of the stat
        /// </summary>
        public float BaseValue;

        /// <summary>
        /// The current value of the stat
        /// </summary>
        public float CurrentValue;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_name">Name of the stat</param>
        public Stat(string _name)
        {
            Name = _name;
        }

        /// <summary>
        /// Alternate constructor
        /// </summary>
        /// <param name="_name">Name of the stat</param>
        /// <param name="_value">Initial value of the stat</param>
        public Stat(string _name, float _value) : this(_name)
        {
            BaseValue = _value;
            CurrentValue = _value;
        }

        #endregion

        #region IStat

        /// <summary>
        /// Get base value
        /// </summary>
        /// <returns>The base value</returns>
        public float GetBase()
        {
            return BaseValue;
        }

        /// <summary>
        /// Get the stat's name
        /// </summary>
        /// <returns>The stat's name</returns>
        public string GetName()
        {
            return Name;
        }

        /// <summary>
        /// Set the base value of a stat
        /// </summary>
        /// <param name="_val">The new base value</param>
        public void SetBase(float _val)
        {
            BaseValue = _val;
        }

        #endregion
    }
}
