﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Common interface for all data classes
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// Initialize this data
        /// </summary>
        void Init();
    }
}
