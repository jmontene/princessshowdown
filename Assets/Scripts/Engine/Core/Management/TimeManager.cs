﻿using UnityEngine;
using Tricolor.System;
using System.Collections;

namespace Tricolor.Engine
{
    public class TimeManager : SingletonBehaviour<TimeManager>
    {
        public void Slowdown(float factor, float duration)
        {
            Time.timeScale = factor;
            StartCoroutine(SlowDownCo(duration));
        }

        IEnumerator SlowDownCo(float duration)
        {
            float timeSinceStart = Time.realtimeSinceStartup;
            while (Time.realtimeSinceStartup < timeSinceStart + duration){
                yield return null;
            }

            Time.timeScale = 1f;
        }
    }
}
