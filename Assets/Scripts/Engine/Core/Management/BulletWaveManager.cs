﻿using Tricolor.System;
using UnityEngine;

namespace Tricolor.Engine
{
    public class BulletWaveManager : SingletonBehaviour<BulletWaveManager>
    {
        [Header("Targetting")]
        public Transform target;
        public float distanceFromTarget;
        public float verticalRadius;
        public float yOffset;

        [Header("Bullet")]
        public float bulletDistance;
        public ProjectileActionSpec bulletSpec;
        public AudioClip waveSound;

        [Header("Timing")]
        public float interval = 3f;
        public float delay = 0f;

        protected ActorController actorController;
        protected Timer waveTimer;
        protected Timer delayTimer;

        protected override void Awake()
        {
            base.Awake();
            actorController = GetComponent<ActorController>();
        }

        protected void Start()
        {
            waveTimer = new Timer(ShootWaveInterval, interval);
            delayTimer = new Timer(ShootWaveInterval, delay);
        }

        public void ShootWave()
        {
            AudioManager.Instance.PlaySFX(waveSound, 3f);
            float curDistance = bulletDistance;
            Vector3 curPos = target.position + (Vector3.right * distanceFromTarget) + (Vector3.up * yOffset);
            SpawnBullet(curPos);

            while(curDistance <= verticalRadius)
            {
                curPos.y += curDistance;
                SpawnBullet(curPos);

                curPos.y -= curDistance * 2;
                SpawnBullet(curPos);

                curPos.y += curDistance;
                curDistance += bulletDistance;
            }
        }

        public void ShootWaveInterval()
        {
            ShootWave();
            waveTimer.Start();
        }

        public void StartInterval()
        {
            delayTimer.Start();
        }

        public void StopInterval()
        {
            waveTimer.Stop();
        }

        public void SetInterval(float _interval)
        {
            waveTimer.SetDuration(_interval);
        }

        public void SetDelay(float _delay)
        {
            delayTimer.SetDuration(_delay);
        }

        protected void SpawnBullet(Vector3 position)
        {
            var fireball = PoolerManager.Instance.projectilePooler.GetObject().GetComponent<ProjectileController>();
            if (fireball == null) return;
            fireball.transform.position = position;
            fireball.CurrentActionSpec = bulletSpec;
            fireball.Shooter = actorController;
            fireball.metaData.MovementData.SetDirection(Vector2.left);
            fireball.metaData.MovementData.MovementSpeed = bulletSpec.moveSpeed;
            fireball.gameObject.SetActive(true);
        }
    }
}
