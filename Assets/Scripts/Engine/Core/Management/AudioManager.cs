﻿using Tricolor.System;
using UnityEngine;
using DG.Tweening;

namespace Tricolor.Engine
{
    public class AudioManager : SingletonBehaviour<AudioManager>
    {

        #region Protected Members

        protected AudioSource audioSource;
        protected float originalVolume;

        protected bool fading;
        protected Timer fadeTimer;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            audioSource = GetComponent<AudioSource>();
            originalVolume = audioSource.volume;
            fading = false;
        }

        protected void Start()
        {
            fadeTimer = new Timer(ResetFading, 0f);
        }

        #endregion

        #region Public Methods

        public void PlayBGM(AudioClip clip)
        {
            audioSource.volume = originalVolume;
            audioSource.clip = clip;
            audioSource.Play();
        }

        public void FadeOut(float duration)
        {
            fading = true;
            audioSource.DOFade(0f, duration);
            fadeTimer.SetDuration(duration);
            fadeTimer.Start();
        }

        public void PlaySFX(AudioClip clip, float volumeScale)
        {
            if (fading) return;
            audioSource.volume = originalVolume;
            audioSource.PlayOneShot(clip, volumeScale);
        }

        protected void ResetFading()
        {
            audioSource.Stop();
            fading = false;
        }

        #endregion
    }
}
