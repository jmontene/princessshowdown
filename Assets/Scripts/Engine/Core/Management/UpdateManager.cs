﻿using UnityEngine;
using Tricolor.System;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Basic Update Manager to localize all updates in the same place
    /// </summary>
    public class UpdateManager : SingletonBehaviour<UpdateManager>
    {
        #region Protected Members

        /// <summary>
        /// Entities to update
        /// </summary>
        protected List<IEntity> entities;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            entities = new List<IEntity>();
        }

        /// <summary>
        /// Main update loop
        /// </summary>
        protected void Update()
        {
            foreach(IEntity entity in entities)
            {
                if(entity.IsActive()) entity.EntityUpdate();
            }
        }

        /// <summary>
        /// Main physics step loop
        /// </summary>
        protected void FixedUpdate()
        {
            foreach (IEntity entity in entities)
            {
                if (entity.IsActive()) entity.EntityFixedUpdate();
            }
        }

        /// <summary>
        /// Update loop used for cleanup after the frame
        /// </summary>
        protected void LateUpdate()
        {
            foreach(IEntity entity in entities)
            {
                if (entity.IsActive()) entity.EntityLateUpdate();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add an entity to the manager
        /// </summary>
        /// <param name="entity">The entity to add</param>
        public void AddEntity(IEntity entity)
        {
            entities.Add(entity);
        }

        /// <summary>
        /// Remove an entity from the manager
        /// </summary>
        /// <param name="entity">The entity to remove</param>
        public void RemoveEntity(IEntity entity)
        {
            entities.Remove(entity);
        }

        #endregion
    }
}
