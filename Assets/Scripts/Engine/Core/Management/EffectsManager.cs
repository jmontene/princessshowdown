﻿using UnityEngine;
using UnityEngine.UI;
using Tricolor.System;

namespace Tricolor.Engine
{
    public class EffectsManager : SingletonBehaviour<EffectsManager>
    {
        #region Public Members

        public Animator FlashAnimator;

        #endregion

        #region Public Methods

        public void Flash()
        {
            FlashAnimator.SetTrigger("Flash");
        }

        #endregion

    }
}
