﻿using Tricolor.System;
using UnityEngine;
using UnityEngine.UI;

namespace Tricolor.Engine
{
    public class ScoringManager : SingletonBehaviour<ScoringManager>
    {
        public int magicOrbsGathered = 0;
        public Text magicOrbsText;

        public AudioClip magicOrbGatherClip;

        public void AddMagicOrb()
        {
            magicOrbsGathered++;
            magicOrbsText.text = magicOrbsGathered.ToString();
            AudioManager.Instance.PlaySFX(magicOrbGatherClip, 3f);
        }

        public int GetNumMagicOrbs()
        {
            return magicOrbsGathered;
        }
    }
}
