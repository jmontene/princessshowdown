﻿using Tricolor.System;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Tricolor.Engine
{

    /// <summary>
    /// Manager for handling all incoming input
    /// </summary>
    public class InputManager : SingletonBehaviour<InputManager>
    {
        #region Public Members

        /// <summary>
        /// Input configuration
        /// </summary>
        public InputConfig inputConfig;

        #endregion

        #region Protected Members

        /// <summary>
        /// Dictionary of all the input listeners for buttons to track
        /// </summary>
        protected Dictionary<ButtonNames, InputListener> buttonInputs;

        #endregion

        #region Reusable Input Args

        /// <summary>
        /// Empty event args for events
        /// </summary>
        EventArgs emptyEventArgs;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            buttonInputs = new Dictionary<ButtonNames, InputListener>();
            buttonInputs.Add(ButtonNames.JUMP, new InputListener(new InputButtonData(inputConfig.Jump)));
            buttonInputs.Add(ButtonNames.ATTACK, new InputListener(new InputButtonData(inputConfig.Attack)));
            buttonInputs.Add(ButtonNames.SWITCH, new InputListener(new InputButtonData(inputConfig.Switch)));
            buttonInputs.Add(ButtonNames.DASH, new InputListener(new InputButtonData(inputConfig.Dash)));
            buttonInputs.Add(ButtonNames.SHIELD, new InputListener(new InputButtonData(inputConfig.Shield)));

            emptyEventArgs = new EventArgs();
        }

        /// <summary>
        /// Update every frame
        /// </summary>
        protected void Update()
        {
            ProcessButtonEvents();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get an axis
        /// </summary>
        /// <param name="axis">The axis to look for</param>
        /// <returns>The value of the axis</returns>
        public float GetAxis(string axis)
        {
            return Input.GetAxisRaw(axis);
        }

        /// <summary>
        /// Determine if a button is pressed
        /// </summary>
        /// <param name="buttonName">The button to check</param>
        /// <returns>Whether the button is pressed or not</returns>
        public bool IsButtonPressed(ButtonNames buttonName)
        {
            return buttonInputs[buttonName].inputData.IsPressed();
        }

        /// <summary>
        /// Subscribe to input pressed event
        /// </summary>
        /// <param name="buttonName">The button to subscribe to</param>
        /// <param name="handler">The event handler</param>
        public void SubscribeToPressedEvent(ButtonNames buttonName, EventHandler handler)
        {
            buttonInputs[buttonName].InputPressedEvent += handler;
        }

        /// <summary>
        /// Subscribe to input released event
        /// </summary>
        /// <param name="buttonName">The button to subscribe to</param>
        /// <param name="handler">The event handler</param>
        public void SubscribeToReleasedEvent(ButtonNames buttonName, EventHandler handler)
        {
            buttonInputs[buttonName].InputReleasedEvent += handler;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Process all button events
        /// </summary>
        protected void ProcessButtonEvents()
        {
            foreach(InputListener listener in buttonInputs.Values)
            {
                if (listener.inputData.HasJustBeenPressed())
                {
                    listener.OnInputPressed(this, emptyEventArgs);
                }

                if (listener.inputData.HasJustBeenReleased())
                {
                    listener.OnInputReleased(this, emptyEventArgs);
                }
            }
        }

        #endregion
    }
}
