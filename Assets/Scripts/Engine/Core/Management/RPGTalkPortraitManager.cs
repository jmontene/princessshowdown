﻿using UnityEngine;
using System.Collections.Generic;
using RPGTALK.Helper;
using UnityEngine.UI;

namespace Tricolor.Engine
{
    public class RPGTalkPortraitManager : MonoBehaviour
    {
        [Header("Basic Parameters")]
        public RPGTalk rpgTalk;
        public Image roseImage;
        public Image serenaImage;
        public Color standByColor;
        public Color nullColor;

        [Header("Rose Images")]
        public Sprite roseNormal;
        public Sprite roseSad;
        public Sprite roseSmug;
        public Sprite roseNormalCrown;
        public Sprite roseSadCrown;
        public Sprite roseSmugCrown;

        [Header("Serena Images")]
        public Sprite serenaSmug;
        public Sprite serenaCurious;
        public Sprite serenaDefeat;
        public Sprite serenaSurprise;
        public Sprite serenaSmugCrown;
        public Sprite serenaCuriousCrown;
        public Sprite serenaDefeatCrown;
        public Sprite serenaSurpriseCrown;
        public Sprite serenaSmugDualCrown;
        public Sprite serenaCuriousDualCrown;
        public Sprite serenaDefeatDualCrown;
        public Sprite serenaSurpriseDualCrown;

        protected bool isRosePresent;
        protected bool isSerenaPresent;
        protected int talkIndex;
        protected List<Sprite> rosePortraits;
        protected List<Sprite> serenaPortraits;

        protected void Awake()
        {
            rpgTalk.OnTalkInitialized += TalkInitialized;
            rpgTalk.OnNewTalk += TalkStarted;
            rpgTalk.OnLineStarted += AdvancePortrait;
        }

        protected void TalkStarted()
        {
            talkIndex = 0;
            isRosePresent = false;
            isSerenaPresent = false;
            roseImage.color = nullColor;
            serenaImage.color = nullColor;
        }

        protected void TalkInitialized()
        {
            rosePortraits = new List<Sprite>();
            serenaPortraits = new List<Sprite>();
            for(int i = 0; i < rpgTalk.rpgtalkElements.Count; ++i)
            {
                SetPortrait(rpgTalk.rpgtalkElements[i]);
            }
            AdvancePortrait();
        }

        protected void SetPortrait(RpgtalkElement elem)
        {
            string speaker = elem.speakerName;

            if (speaker.StartsWith("Rose"))
            {
                isRosePresent = true;
                elem.speakerName = "Rose";
                switch (speaker)
                {
                    case "Rose_Normal":
                        rosePortraits.Add(roseNormal);
                        break;
                    case "Rose_Sad":
                        rosePortraits.Add(roseSad);
                        break;
                    case "Rose_Smug":
                        rosePortraits.Add(roseSmug);
                        break;
                    case "Rose_Normal_Crown":
                        rosePortraits.Add(roseNormalCrown);
                        break;
                    case "Rose_Sad_Crown":
                        rosePortraits.Add(roseSadCrown);
                        break;
                    case "Rose_Smug_Crown":
                        rosePortraits.Add(roseSmugCrown);
                        break;
                }
                serenaPortraits.Add(serenaCurious);
            }else if (speaker.StartsWith("Serena"))
            {
                isSerenaPresent = true;
                elem.speakerName = "Serena";
                switch (speaker)
                {
                    case "Serena_Smug":
                        serenaPortraits.Add(serenaSmug);
                        break;
                    case "Serena_Curious":
                        serenaPortraits.Add(serenaCurious);
                        break;
                    case "Serena_Defeated":
                        serenaPortraits.Add(serenaDefeat);
                        break;
                    case "Serena_Surprise":
                        serenaPortraits.Add(serenaSurprise);
                        break;
                    case "Serena_Smug_Crown":
                        serenaPortraits.Add(serenaSmugCrown);
                        break;
                    case "Serena_Curious_Crown":
                        serenaPortraits.Add(serenaCuriousCrown);
                        break;
                    case "Serena_Defeated_Crown":
                        serenaPortraits.Add(serenaDefeatCrown);
                        break;
                    case "Serena_Surprise_Crown":
                        serenaPortraits.Add(serenaSurpriseCrown);
                        break;
                    case "Serena_Smug_DualCrown":
                        serenaPortraits.Add(serenaSmugDualCrown);
                        break;
                    case "Serena_Curious_DualCrown":
                        serenaPortraits.Add(serenaCuriousDualCrown);
                        break;
                    case "Serena_Defeated_DualCrown":
                        serenaPortraits.Add(serenaDefeatDualCrown);
                        break;
                    case "Serena_Surprise_DualCrown":
                        serenaPortraits.Add(serenaSurpriseDualCrown);
                        break;
                }
                rosePortraits.Add(roseNormal);
            }
        }

        protected void AdvancePortrait()
        {
            if (talkIndex >= rpgTalk.rpgtalkElements.Count) return;
            RpgtalkElement elem = rpgTalk.rpgtalkElements[talkIndex];
            if (elem.speakerName.StartsWith("Rose")){
                roseImage.sprite = rosePortraits[talkIndex];
                roseImage.color = Color.white;
                if (isSerenaPresent)
                {
                    serenaImage.color = standByColor;
                }
            }else if (elem.speakerName.StartsWith("Serena"))
            {
                serenaImage.sprite = serenaPortraits[talkIndex];
                serenaImage.color = Color.white;
                if (isRosePresent)
                {
                    roseImage.color = standByColor;
                }
            }
            ++talkIndex;
        }
    }
}
