﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data class for input buttons
    /// </summary>
    public class InputAxisData : IInputData
    {
        #region Public Members

        /// <summary>
        /// The name of the button
        /// </summary>
        protected string AxisName;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_name">The name of the button</param>
        public InputAxisData(string _name)
        {
            AxisName = _name;
        }

        #endregion

        #region IInputData Interface

        /// <summary>
        /// Get the value of this input
        /// For axis, is the current axis value
        /// For buttons, it is 1 if pressed, 0 if not
        /// </summary>
        /// <returns>The current value of this input</returns>
        public float GetValue()
        {
            return Input.GetAxisRaw(AxisName);
        }

        /// <summary>
        /// Determines whether this input is pressed
        /// Returns false for axes
        /// </summary>
        /// <returns></returns>
        public bool HasJustBeenPressed()
        {
            return false;
        }

        /// <summary>
        /// Determines if the input has been pressed this frame
        /// </summary>
        /// <returns></returns>
        public bool HasJustBeenReleased()
        {
            return false;
        }

        /// <summary>
        /// Determines if the input has been released this frame
        /// </summary>
        /// <returns></returns>
        public bool IsPressed()
        {
            return false;
        }

        #endregion
    }
}
