﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Data class for input buttons
    /// </summary>
    public class InputButtonData : IInputData
    {
        #region Protected Members

        /// <summary>
        /// The name of the button
        /// </summary>
        protected KeyCode[] ButtonKeys;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_name">The name of the button</param>
        public InputButtonData(KeyCode[] _keys)
        {
            ButtonKeys = _keys;
        }

        #endregion

        #region IInputData Interface

        /// <summary>
        /// Get the value of this input
        /// For axis, is the current axis value
        /// For buttons, it is 1 if pressed, 0 if not
        /// </summary>
        /// <returns>The current value of this input</returns>
        public float GetValue()
        {
            foreach(KeyCode key in ButtonKeys)
            {
                if (Input.GetKey(key)) return 1;
            }
            return 0;
        }

        /// <summary>
        /// Determines whether this input is pressed
        /// Returns false for axes
        /// </summary>
        /// <returns></returns>
        public bool HasJustBeenPressed()
        {
            foreach (KeyCode key in ButtonKeys)
            {
                if (Input.GetKeyDown(key)) return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if the input has been pressed this frame
        /// </summary>
        /// <returns></returns>
        public bool HasJustBeenReleased()
        {
            foreach (KeyCode key in ButtonKeys)
            {
                if (Input.GetKeyUp(key)) return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if the input has been released this frame
        /// </summary>
        /// <returns></returns>
        public bool IsPressed()
        {
            foreach (KeyCode key in ButtonKeys)
            {
                if (Input.GetKey(key)) return true;
            }
            return false;
        }

        #endregion
    }
}
