﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Enumeration for axis names
    /// </summary>
    public enum AxisNames
    {
        HORIZONTAL,
        VERTICAL
    }

    /// <summary>
    /// Enumeration for button names
    /// </summary>
    public enum ButtonNames
    {
        JUMP,
        ATTACK,
        SWITCH,
        DASH,
        SHIELD
    }

    /// <summary>
    /// A collection of strings to use as axis names
    /// </summary>
    public class AxisStrings
    {
        public static readonly string HORIZONTAL = "Horizontal";
        public static readonly string VERTICAL = "Vertical";

        public static readonly string[] AXISNAMES =
        {
            HORIZONTAL,
            VERTICAL
        };
    }
}
