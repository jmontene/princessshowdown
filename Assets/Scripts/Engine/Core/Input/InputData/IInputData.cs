﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all input data
    /// elements
    /// </summary>
    public interface IInputData
    {
        /// <summary>
        /// Get the value of this input
        /// For axis, is the current axis value
        /// For buttons, it is 1 if pressed, 0 if not
        /// </summary>
        /// <returns>The current value of this input</returns>
        float GetValue();

        /// <summary>
        /// Determines whether this input is pressed
        /// Returns false for axesadon
        /// </summary>
        /// <returns></returns>
        bool IsPressed();

        /// <summary>
        /// Determines if the input has been pressed this frame
        /// </summary>
        /// <returns></returns>
        bool HasJustBeenPressed();

        /// <summary>
        /// Determines if the input has been released this frame
        /// </summary>
        /// <returns></returns>
        bool HasJustBeenReleased();
    }
}
