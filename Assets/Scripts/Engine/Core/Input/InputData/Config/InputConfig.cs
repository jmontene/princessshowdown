﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Configuration for input
    /// </summary>
    [Serializable]
    [CreateAssetMenu(fileName ="InputConfig", menuName = "Tricolor/Input/InputConfig", order = 1)]
    public class InputConfig : ScriptableObject
    {
        #region Public Members

        [Header("Buttons")]
        public KeyCode[] Jump;
        public KeyCode[] Attack;
        public KeyCode[] Switch;
        public KeyCode[] Dash;
        public KeyCode[] Shield;

        #endregion
    }
}
