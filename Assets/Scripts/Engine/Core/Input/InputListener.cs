﻿using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Object that contains input events
    /// </summary>
    public class InputListener
    {
        #region Public Members

        /// <summary>
        /// Input data tracked
        /// </summary>
        public IInputData inputData;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_data">The input data to carry</param>
        public InputListener(IInputData _data)
        {
            inputData = _data;
        }

        #endregion

        #region Events

        /// <summary>
        /// Input pressed event
        /// </summary>
        public event EventHandler InputPressedEvent;

        /// <summary>
        /// Input released event
        /// </summary>
        public event EventHandler InputReleasedEvent;

        #endregion

        #region Public Methods

        /// <summary>
        /// Fire the Input Pressed event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event arguments</param>
        public void OnInputPressed(object _sender, EventArgs _emptyEventArgs)
        {
            InputPressedEvent?.Invoke(_sender, _emptyEventArgs);
        }

        /// <summary>
        /// Fire the Input Released event
        /// </summary>
        /// <param name="_sender">Sender of this event</param>
        /// <param name="_emptyEventArgs">Empty event arguments</param>
        public void OnInputReleased(object _sender, EventArgs _emptyEventArgs)
        {
            InputReleasedEvent?.Invoke(_sender, _emptyEventArgs);
        }

        #endregion
    }
}
