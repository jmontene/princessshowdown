﻿using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Base controller class for all the objects in the game
    /// </summary>
    public abstract class EntityControllerBase : MonoBehaviour, IEntityController
    {
        #region Protected Members

        /// <summary>
        /// The behaviours to manage
        /// </summary>
        protected List<IEntity> behaviours;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialize components and references
        /// </summary>
        protected virtual void Awake()
        {
            behaviours = new List<IEntity>();
        }

        /// <summary>
        /// Initialization behaviour that depends on other
        /// Awake methods
        /// </summary>
        protected virtual void Start()
        {
            UpdateManager.Instance.AddEntity(this);
        }

        #endregion

        #region IEntityController Interface

        /// <summary>
        /// Add a behaviour to this controller
        /// </summary>
        /// <param name="behaviour">The behaviour to add</param>
        public void AddBehaviour(IEntity behaviour)
        {
            behaviours.Add(behaviour);
        }

        /// <summary>
        /// Remove a behaviour from this controller
        /// </summary>
        /// <param name="behaviour">The behaviour to remove</param>
        public void RemoveBehaviour(IEntity behaviour)
        {
            behaviours.Remove(behaviour);
        }

        #endregion

        #region IEntity Interface

        /// <summary>
        /// Unity update
        /// </summary>
        public void EntityUpdate()
        {
            foreach(IEntity entity in behaviours)
            {
                if (entity.IsActive()) entity.EntityUpdate();
            }
        }

        /// <summary>
        /// Unity Fixed Update
        /// </summary>
        public void EntityFixedUpdate()
        {
            foreach (IEntity entity in behaviours)
            {
                if (entity.IsActive()) entity.EntityFixedUpdate();
            }
        }

        /// <summary>
        /// Unity Late Update
        /// </summary>
        public void EntityLateUpdate()
        {
            foreach(IEntity entity in behaviours)
            {
                if (entity.IsActive()) entity.EntityLateUpdate();
            }
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        #endregion

    }
}
