﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all Entity Controllers
    /// </summary>
    public interface IEntityController : IEntity
    {
        void AddBehaviour(IEntity behaviour);

        void RemoveBehaviour(IEntity behaviour);
    }
}
