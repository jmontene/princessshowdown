﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all behaviours
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Unity Update method
        /// </summary>
        void EntityUpdate();

        /// <summary>
        /// Unity FixedUpdate method
        /// </summary>
        void EntityFixedUpdate();

        /// <summary>
        /// Unity LateUpdate method
        /// </summary>
        void EntityLateUpdate();

        /// <summary>
        /// Determines if the entity is active
        /// </summary>
        /// <returns>Whether the entity is active</returns>
        bool IsActive();
    }
}
