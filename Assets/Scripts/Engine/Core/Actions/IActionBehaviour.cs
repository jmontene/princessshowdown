﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Common signature for all action behaviours
    /// </summary>
    public interface IActionBehaviour
    {
        /// <summary>
        /// Execute this action behaviour
        /// </summary>
        void Execute(ActionExecutor executor);

        /// <summary>
        /// Initialize this behaviour with the data of
        /// another. The other behaviour must be 
        /// of the same type
        /// </summary>
        void Init(ActionExecutor executor);
    }
}
