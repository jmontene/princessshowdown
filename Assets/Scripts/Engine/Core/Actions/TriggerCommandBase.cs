﻿using UnityEngine;

namespace Tricolor.Engine
{

    public class TriggerCommandBase : MonoBehaviour
    {
        public GameObject[] triggersToEnable;
        public GameObject[] triggersToDisable;
        public bool disableOnTrigger;

        public void OnTriggerCollided()
        {
            ExecuteTrigger();
            EnableOtherTriggers();

            if (disableOnTrigger)
            {
                gameObject.SetActive(false);
            }
        }

        public virtual void ExecuteTrigger()
        {

        }

        protected void EnableOtherTriggers()
        {
            for (int i = 0; i < triggersToEnable.Length; ++i)
            {
                triggersToEnable[i].SetActive(true);
            }
            for (int i = 0; i < triggersToDisable.Length; ++i)
            {
                triggersToDisable[i].SetActive(false);
            }
        }
    }
}
