﻿using UnityEngine;
using System.Collections.Generic;
using Tricolor.Gameplay;

namespace Tricolor.Engine
{
    /// <summary>
    /// Object that executes actions as
    /// specified by an ActionBehaviour
    /// </summary>
    public class ActionExecutor
    {
        #region Public Accessors

        /// <summary>
        /// This executor's stats
        /// </summary>
        public EntityStats stats { get; protected set; }

        /// <summary>
        /// The controller associated 
        /// with this executor
        /// </summary>
        public ActorController controller { get; set; }

        /// <summary>
        /// Map for timers associated with this executor
        /// </summary>
        public Dictionary<string, ActionExecutorTimer> timers { get; set; }

        /// <summary>
        /// Map for boolean flags
        /// </summary>
        public Dictionary<string, bool> flags { get; set; }

        /// <summary>
        /// The gameObject of this executor;
        /// </summary>
        public GameObject gameObject;

        /// <summary>
        /// Reference to the actor metaData
        /// of the associated gameObject
        /// </summary>
        public ActorMetaData metaData;

        public Transform projectileOrigin;

        public BasicEnemyAI enemyAi;

        public Transform target;

        #endregion

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="_controller">The controller in charge of this executor</param>
        public ActionExecutor(ActorController _controller, GameObject _obj)
        {
            stats = new EntityStats();
            controller = _controller;
            timers = new Dictionary<string, ActionExecutorTimer>();
            flags = new Dictionary<string, bool>();
            gameObject = _obj;
            metaData = gameObject.GetComponent<ActorMetaData>();
        }

        /// <summary>
        /// Registers a timer to this executor
        /// </summary>
        /// <param name="timerName"></param>
        public void RegisterTimer(string key, ActionExecutorTimer timer)
        {
            timer.SetExecutor(this);
            timers.Add(key, timer);
        }

        /// <summary>
        /// Start a timer
        /// </summary>
        /// <param name="key">The key of the timer to start</param>
        public void StartTimer(string key)
        {
            ActionExecutorTimer timer;
            timers.TryGetValue(key, out timer);
            if (timer != null) timer.Start();
        }

        /// <summary>
        /// Start a timer
        /// </summary>
        /// <param name="key">The key of the timer to start</param>
        public void StopTimer(string key)
        {
            ActionExecutorTimer timer;
            timers.TryGetValue(key, out timer);
            if (timer != null) timer.Stop();
        }

        public void AddFlag(string key, bool value)
        {
            flags.Add(key, value);
        }

        public void SetFlag(string key, bool value)
        {
            flags[key] = value;
        }

        public bool GetFlag(string key)
        {
            return flags[key];
        }
    }
}
