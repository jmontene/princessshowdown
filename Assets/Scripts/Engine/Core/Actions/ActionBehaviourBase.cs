﻿using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    /// <summary>
    /// Base class for all action behaviours
    /// </summary>
    public abstract class ActionBehaviourBase : ScriptableObject, IActionBehaviour
    {

        #region IActionBehaviour

        /// <summary>
        /// Execute this action behaviour on an executor
        /// <param name="executor">The executor</param>
        /// </summary>
        public abstract void Execute(ActionExecutor executor);

        /// <summary>
        /// Initialize this behaviour with the data of
        /// another. The other behaviour must be 
        /// of the same type
        /// </summary>
        /// <param name="executor">The executor to initialize</param>
        public abstract void Init(ActionExecutor executor);

        #endregion

    }
}
