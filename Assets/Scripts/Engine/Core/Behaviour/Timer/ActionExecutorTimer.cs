﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Basic timer object
    /// </summary>
    public class ActionExecutorTimer : IEntity
    {
        #region Protected Members

        /// <summary>
        /// The callback upon finishing the timer
        /// </summary>
        protected Action<ActionExecutor> callback;

        /// <summary>
        /// The duration of the timer
        /// </summary>
        protected float duration;

        /// <summary>
        /// Determines if the timer is running or not
        /// </summary>
        protected bool running = false;

        /// <summary>
        /// The time that has elapsed for this timer
        /// </summary>
        protected float timeElapsed = 0f;

        /// <summary>
        /// The action executor the timer function is subjected by
        /// </summary>
        protected ActionExecutor executor;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="callback"></param>
        public ActionExecutorTimer(Action<ActionExecutor> _callback, float _duration)
        {
            callback = _callback;
            duration = _duration;
            UpdateManager.Instance.AddEntity(this);
        }

        #endregion

        #region Public Methods

        public void SetExecutor(ActionExecutor _executor)
        {
            executor = _executor;
        }

        public void Start()
        {
            running = true;
            timeElapsed = 0f;
        }

        public void Stop()
        {
            running = false;
        }

        #endregion

        #region IEntity

        /// <summary>
        /// Update step
        /// </summary>
        public void EntityUpdate()
        {
            if (running)
            {
                timeElapsed += Time.deltaTime;
                if (timeElapsed >= duration)
                {
                    Stop();
                    callback(executor);
                }
            }
        }

        /// <summary>
        /// FixedUpdate step
        /// </summary>
        public void EntityFixedUpdate()
        {
        }

        /// <summary>
        /// Late Update Step
        /// </summary>
        public void EntityLateUpdate()
        {
        }

        public bool IsActive()
        {
            return true;
        }

        #endregion
    }
}
