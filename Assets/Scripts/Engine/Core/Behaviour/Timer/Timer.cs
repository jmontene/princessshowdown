﻿using System;
using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Basic timer object
    /// </summary>
    public class Timer : IEntity
    {
        #region Protected Members

        /// <summary>
        /// The callback upon finishing the timer
        /// </summary>
        Action callback;

        /// <summary>
        /// The duration of the timer
        /// </summary>
        float duration;

        /// <summary>
        /// Determines if the timer is running or not
        /// </summary>
        bool running = false;

        /// <summary>
        /// The time that has elapsed for this timer
        /// </summary>
        float timeElapsed = 0f;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="callback"></param>
        public Timer(Action _callback, float _duration)
        {
            callback = _callback;
            duration = _duration;
            UpdateManager.Instance.AddEntity(this);
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            running = true;
            timeElapsed = 0f;
        }

        public void Stop()
        {
            running = false;
        }

        public void SetDuration(float _duration)
        {
            duration = _duration;
        }

        #endregion

        #region IEntity

        /// <summary>
        /// Update step
        /// </summary>
        public void EntityUpdate()
        {
            if (running)
            {
                timeElapsed += Time.deltaTime;
                if(timeElapsed >= duration)
                {
                    Stop();
                    callback();
                }
            }
        }


        /// <summary>
        /// FixedUpdate step
        /// </summary>
        public void EntityFixedUpdate()
        {
        }

        /// <summary>
        /// Late Update Step
        /// </summary>
        public void EntityLateUpdate()
        {
        }

        public bool IsActive()
        {
            return true;
        }

        #endregion
    }
}
