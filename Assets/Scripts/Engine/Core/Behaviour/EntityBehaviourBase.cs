﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Base class for all behaviours
    /// </summary>
    public abstract class EntityBehaviourBase : MonoBehaviour, IEntity
    {
        #region Protected Members

        /// <summary>
        /// The controller this behaviour is associated to
        /// </summary>
        protected IEntityController controller;

        /// <summary>
        /// The metadata associated to
        /// this object
        /// </summary>
        protected IMetaData metaData;

        #endregion

        #region Unity Methods

        /// <summary>
        /// Initialization that depends on other objects
        /// </summary>
        protected virtual void Awake()
        {
            GetController();
            GetMetaData();
        }

        /// <summary>
        /// Initialization that depends on other objects
        /// </summary>
        protected virtual void Start()
        {
            controller.AddBehaviour(this);
        }

        #endregion

        #region IEntity Interface

        /// <summary>
        /// Unity Update Method
        /// </summary>
        public virtual void EntityUpdate() { }

        /// <summary>
        /// Unity FixedUpdate Method
        /// </summary>
        public virtual void EntityFixedUpdate() { }

        /// <summary>
        /// Unity LateUpdate method
        /// </summary>
        public virtual void EntityLateUpdate() { }

        /// <summary>
        /// Determines if this object is active
        /// </summary>
        /// <returns>Whether this object is active</returns>
        public virtual bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Obtain and assign the associated controller
        /// </summary>
        protected virtual void GetController()
        {
            controller = GetComponent<IEntityController>();
        }

        protected virtual void GetMetaData()
        {
            metaData = GetComponent<IMetaData>();
        }

        #endregion
    }
}
