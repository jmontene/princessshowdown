﻿namespace Tricolor.Engine
{
    /// <summary>
    /// Manage interactions between a projectile
    /// and other actors
    /// </summary>
    public class ProjectileInteractionBehaviour : ActorInteractionBehaviour
    {
        #region Protected Members

        protected ProjectileController projectileController;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            projectileController = GetComponent<ProjectileController>();
        }

        #endregion

        #region ActorInteractionBehaviour Overrides

        protected override void ProcessCollision(ActorController other)
        {
            base.ProcessCollision(other);
            var spec = projectileController.CurrentActionSpec;
            var shooter = projectileController.Shooter;
            spec.OnHit(projectileController, shooter, other);
        }

        #endregion
    }
}
