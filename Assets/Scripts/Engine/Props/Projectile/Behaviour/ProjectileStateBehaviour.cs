﻿using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Behaviour that manages
    /// projectile state
    /// </summary>
    public class ProjectileStateBehaviour : ActorStateBehaviour
    {

        public float timeToDie;

        #region Protected Members

        /// <summary>
        /// The death timer for this bullet
        /// </summary>
        protected Timer deathTimer;

        /// <summary>
        /// Reference to the projectile controller
        /// </summary>
        protected ProjectileController projectileController;

        #endregion

        #region Reusable Event Args

        /// <summary>
        /// Die event args
        /// </summary>
        protected EventArgs dieEventArgs;

        #endregion

        #region Unity Methods

        protected override void Start()
        {
            base.Awake();

            projectileController = GetComponent<ProjectileController>();

            deathTimer = new Timer(TriggerDeath, timeToDie);
            deathTimer.Start();

            dieEventArgs = new EventArgs();
            actorController.DieEvent += HandleDie;
        }

        protected void OnEnable()
        {
            if(deathTimer != null) deathTimer.Start();
        }

        protected void OnDisable()
        {
            if (deathTimer != null) deathTimer.Stop();
        }

        #endregion

        #region Protected Methods

        protected void Die()
        {
            projectileController.EmptyHitList();
            gameObject.SetActive(false);
        }

        protected void TriggerDeath()
        {
            actorController.Die(this, dieEventArgs);
        }

        #endregion

        #region Event Handlers

        protected void HandleDie(object _sender, EventArgs _emptyEventArgs)
        {
            Die();
        }

        #endregion
    }
}
