﻿using UnityEngine;

namespace Tricolor.Engine
{
    /// <summary>
    /// Container for Projectile Data and data transformation events
    /// </summary>
    public class ProjectileMetaData : MetaDataBase
    {
        #region Inspector Members

        [Header("Actor")]

        ///<summary>
        /// Movement Data
        /// </summary>
        public ActorMovementData MovementData;

        #endregion

        #region MetaDataBase

        /// <summary>
        /// Initialize the data contained here
        /// </summary>
        protected override void InitializeData()
        {
            base.InitializeData();

            data.Add(MovementData);
        }

        #endregion
    }
}
