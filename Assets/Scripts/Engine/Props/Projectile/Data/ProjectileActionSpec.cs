﻿using UnityEngine;
using System;

namespace Tricolor.Engine
{
    /// <summary>
    /// Class for specification details on projectiles
    /// </summary>
    [CreateAssetMenu(fileName = "ProjectileActionSpec", menuName = "Tricolor/Actions/Projectile/ProjectileActionSpec", order = 1)]
    public class ProjectileActionSpec : ScriptableObject
    {
        #region Public Methods

        /// <summary>
        /// The sprite of this projectile
        /// </summary>
        public Sprite sprite;

        /// <summary>
        /// The base damage of this projectile
        /// </summary>
        public float baseDamage;

        /// <summary>
        /// Base move speed of the projectile
        /// </summary>
        public float moveSpeed;

        public AudioClip shotSound;

        #endregion

        #region Reusable Event Args

        /// <summary>
        /// Empty event args for events
        /// that don't specifically require
        /// event arguments
        /// </summary>
        protected EventArgs emptyEventArgs;

        #endregion

        #region Unity Methods

        protected virtual void Awake()
        {
            emptyEventArgs = new EventArgs();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Behaviour to run when hitting an actor with the bullet
        /// </summary>
        /// <param name="thisProjectile">The projectile that is being shot</param>
        /// <param name="shooter">The actor who shot the bullet</param>
        /// <param name="receiver">The receiver of the bullet</param>
        public virtual void OnHit(ProjectileController thisProjectile, ActorController shooter, ActorController receiver)
        {
            if (receiver.shielded)
            {
                thisProjectile.Die(this, emptyEventArgs);
                return;
            }
            float shooterAttack = shooter.GetStat(ActorStrings.STAT_ATTACK);
            float attackValue = shooterAttack + baseDamage;
            receiver.DealDamage(this, new DealDamageEventArgs(attackValue));
            thisProjectile.Die(this, emptyEventArgs);
        }

        #endregion
    }
}
