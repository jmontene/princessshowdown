﻿using UnityEngine;
using System.Collections.Generic;

namespace Tricolor.Engine
{
    public class ProjectileController : ActorController
    {
        #region Public Members

        /// <summary>
        /// The projectile's current action spec
        /// </summary>
        [HideInInspector]
        public ProjectileActionSpec CurrentActionSpec;

        /// <summary>
        /// The shooter of this projectile
        /// </summary>
        [HideInInspector]
        public ActorController Shooter;

        [HideInInspector]
        public List<GameObject> hitObjects;

        #endregion

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            hitObjects = new List<GameObject>();
        }

        #endregion

        #region Public methods

        public void EmptyHitList()
        {
            hitObjects.Clear();
        }

        #endregion
    }
}
