﻿using UnityEngine;

namespace Tricolor.Engine
{
    public class Bouncer : MonoBehaviour
    {
        public AudioClip bounceSound;
        public float refreshCooldown = 0.1f;

        protected Animator anim;
        protected Collider2D collider2d;
        protected Timer coolDownTimer;

        protected void Awake()
        {
            anim = GetComponent<Animator>();
            collider2d = GetComponent<Collider2D>();
        }

        protected void Start()
        {
            coolDownTimer = new Timer(RefreshBounce, refreshCooldown);
        }

        public void Bounce()
        {
            collider2d.enabled = false;
            coolDownTimer.Start();
            AudioManager.Instance.PlaySFX(bounceSound, 3f);
            anim.SetTrigger("Bounce");
        }

        protected void RefreshBounce()
        {
            collider2d.enabled = true;
        }
    }
}
