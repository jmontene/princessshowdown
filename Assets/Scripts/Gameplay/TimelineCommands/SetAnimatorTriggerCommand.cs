﻿using UnityEngine;
using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class SetAnimatorTriggerCommand : MonoBehaviour
    {
        public Animator anim;
        public string triggerName;

        protected void OnEnable()
        {
            anim.SetTrigger(triggerName);
        }
    }
}
