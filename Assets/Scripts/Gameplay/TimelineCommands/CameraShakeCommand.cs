﻿using UnityEngine;
using Tricolor.Engine;
using System.Collections;

namespace Tricolor.Gameplay
{
    public class CameraShakeCommand : MonoBehaviour
    {
        public GameObject targetCam;
        public float timeToShake;

        protected void OnEnable()
        {
            StartCoroutine(DoShake());
        }

        protected IEnumerator DoShake()
        {
            CameraShake shake = targetCam.GetComponent<CameraShake>();
            shake.StartShake();
            yield return new WaitForSeconds(timeToShake);
            shake.StopShake();
        }
    }
}
