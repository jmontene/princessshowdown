﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public enum PlayerGameplaySetting
    {
        SHIELD,
        SWITCH,
        DIRECTION
    }

    public class PlayerChangeSettingCommand : MonoBehaviour
    {
        public PlayerGameplaySetting setting;
        public PlayerController target;
        public bool boolValue;
        public Vector2 vector2Value;

        protected void OnEnable()
        {
            switch (setting)
            {
                case PlayerGameplaySetting.SHIELD:
                    target.playerMetaData.shieldEnabled = boolValue;
                    break;
                case PlayerGameplaySetting.SWITCH:
                    target.playerMetaData.switchEnabled = boolValue;
                    break;
                case PlayerGameplaySetting.DIRECTION:
                    target.metaData.PhysicsData.xGlobalSpeed = target.metaData.MovementData.MovementSpeed;
                    break;
            }
        }
    }
}
