﻿using UnityEngine;
using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class PlayBGMCommand : MonoBehaviour
    {
        public AudioClip audioClip;

        void OnEnable()
        {
            AudioManager.Instance.PlayBGM(audioClip);
        }
    }
}
