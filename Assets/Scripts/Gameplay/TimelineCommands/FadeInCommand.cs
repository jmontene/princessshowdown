﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Tricolor.Gameplay
{
    public class FadeInCommand : MonoBehaviour
    {
        public Image fader;
        public float duration;

        protected void OnEnable()
        {
            fader.DOColor(Color.black, duration);
        }
    }
}
