﻿using Tricolor.Engine;
using UnityEngine;
using System;

namespace Tricolor.Gameplay
{
    public enum PlayerAction
    {
        SHIELD,
        SWITCH
    }

    public class PlayerActionCommand : MonoBehaviour
    {
        public PlayerAction action;
        public PlayerController target;

        protected EventArgs args;

        protected void Awake()
        {
            args = new EventArgs();
        }

        protected void OnEnable()
        {
            switch (action)
            {
                case PlayerAction.SHIELD:
                    target.Shield(this, args);
                    break;
                case PlayerAction.SWITCH:
                    target.Switch(this, args);
                    break;
            }
        }
    }
}
