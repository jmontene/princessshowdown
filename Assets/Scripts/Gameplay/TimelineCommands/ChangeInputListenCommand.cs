﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class ChangeInputListenCommand : MonoBehaviour
    {
        public ActorController controller;
        public bool listensToInput;

        protected void OnEnable()
        {
            controller.metaData.InputData.ListenToInput = listensToInput;
            if (!listensToInput)
            {
                controller.metaData.MovementData.SetDirection(Vector2.zero);
                controller.metaData.PhysicsData.Velocity = Vector2.zero;
            }
            Debug.Log("Change Input Listen Command Enabled");
        }
    }
}
