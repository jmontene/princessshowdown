﻿using Tricolor.Engine;
using Cinemachine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class SetMainCameraCommand : MonoBehaviour
    {
        public CinemachineVirtualCamera targetCamera;

        protected void OnEnable()
        {
            targetCamera.MoveToTopOfPrioritySubqueue();
            GameObject.Find("Rose").GetComponent<PlayerMetaData>().currentCam = targetCamera;
        }
    }
}
