﻿using Tricolor.Engine;
using UnityEngine;
using UnityEngine.Playables;
using RPGTALK.Timeline;

namespace Tricolor.Gameplay
{
    public class StartTimelineCommand : MonoBehaviour
    {
        public PlayableDirector targetDirector;
        public RPGTalkTimeline rpgTalkTimeline;

        protected void OnEnable()
        {
            rpgTalkTimeline.timelineDirector = targetDirector;
            targetDirector.Play();
        }
    }
}
