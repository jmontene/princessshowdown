﻿using UnityEngine;

namespace Tricolor.Gameplay
{
    public class TransformationCutInCommand : MonoBehaviour
    {
        public Animator cutInAnim;

        protected void OnEnable()
        {
            cutInAnim.gameObject.SetActive(true);
            cutInAnim.SetTrigger("CutIn");
        }
    }
}
