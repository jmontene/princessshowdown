﻿using Tricolor.Engine;
using UnityEngine;
using UnityEngine.Playables;
using RPGTALK.Timeline;
using System.Collections;

namespace Tricolor.Gameplay
{
    public class EndingTimelineSwitchCommand : MonoBehaviour
    {
        public PlayableDirector previousDirector;
        public PlayableDirector allOrbsDirector;
        public PlayableDirector noOrbsDirector;
        public RPGTalkTimeline rpgTalkTimeline;

        protected void OnEnable()
        {
            //previousDirector.Stop();
            if(ScoringManager.Instance.magicOrbsGathered == 10)
            {
                rpgTalkTimeline.timelineDirector = allOrbsDirector;
                allOrbsDirector.Play();
            }
            else
            {
                rpgTalkTimeline.timelineDirector = noOrbsDirector;
                noOrbsDirector.Play();
            }
        }
    }
}
