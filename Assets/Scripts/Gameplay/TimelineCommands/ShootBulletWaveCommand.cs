﻿using UnityEngine;
using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class ShootBulletWaveCommand : MonoBehaviour
    {
        public float yOffset;

        protected void OnEnable()
        {
            BulletWaveManager.Instance.yOffset = yOffset;
            BulletWaveManager.Instance.ShootWave();
        }
    }
}
