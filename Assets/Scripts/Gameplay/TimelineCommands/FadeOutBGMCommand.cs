﻿using UnityEngine;
using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class FadeOutBGMCommand : MonoBehaviour
    {
        public float duration = 1f;

        protected void OnEnable()
        {
            AudioManager.Instance.FadeOut(duration);
        }
    }
}
