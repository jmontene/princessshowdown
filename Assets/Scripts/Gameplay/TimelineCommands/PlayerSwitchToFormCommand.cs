﻿using Tricolor.Engine;
using UnityEngine;
using System;

namespace Tricolor.Gameplay
{
    public class PlayerSwitchToFormCommand : MonoBehaviour
    {
        public PlayerController controller;
        public PlayerForm form;

        protected void OnEnable()
        {
            controller.playerMetaData.currentPlayerForm = form;
            controller.Switch(this, new EventArgs());
        }
    }
}
