﻿using UnityEngine;
using DG.Tweening;

namespace Tricolor.Gameplay
{
    public class MoveToWaypointCommand : MonoBehaviour
    {
        public Transform objectToMove;
        public Transform waypoint;
        public float timeToArrive;

        protected void OnEnable()
        {
            objectToMove.DOMove(waypoint.position, timeToArrive);
        }
    }
}
