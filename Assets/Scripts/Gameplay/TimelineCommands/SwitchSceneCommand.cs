﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tricolor.Gameplay
{
    public class SwitchSceneCommand : MonoBehaviour
    {
        public string sceneName;

        protected void OnEnable()
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
