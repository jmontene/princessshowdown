﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class StartSerenaAICommand : MonoBehaviour
    {
        public SerenaAI ai;

        protected void OnEnable()
        {
            ai.StartAI();
        }
    }
}
