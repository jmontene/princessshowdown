﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class DeathCheat : MonoBehaviour
    {
        protected PlayerController playerController;
        protected DealDamageEventArgs args;

        protected void Awake()
        {
            playerController = GetComponent<PlayerController>();
            args = new DealDamageEventArgs(9999);
        }

        protected void Update()
        {
            if (Input.GetButtonDown("Death"))
            {
                playerController.DealDamage(this, args);
            }
        }
    }
}
