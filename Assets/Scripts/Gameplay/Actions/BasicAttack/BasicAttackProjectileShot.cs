﻿using Tricolor.Engine;
using UnityEngine;
using System.Collections;

namespace Tricolor.Gameplay
{
    /// <summary>
    /// A mage's basic attack
    /// Shoot an orb in front of you
    /// </summary>
    [CreateAssetMenu(fileName ="BasicAttack_ProjectileShot", menuName = "Tricolor/Actions/BasicAttack/ProjectileShot", order = 1)]
    public class BasicAttackProjectileShot : ActionBehaviourBase
    {
        #region Constants
        protected static readonly string TIMERKEY = "BasicAttack_ProjectileShot";

        protected static readonly string FLAG_SHOOTING = "Shooting";

        #endregion

        #region Inspector Members

        /// <summary>
        /// Information to set up
        /// the projectile
        /// </summary>
        public ProjectileActionSpec projectileInfo;

        /// <summary>
        /// The rate of fire
        /// A bullet is fired every FireRate seconds
        /// if the attack button is kept pressed
        /// </summary>
        public float FireRate = 0.2f;

        /// <summary>
        /// The move speed of the shot bullet
        /// </summary>
        public float MoveSpeed = 10f;

        #endregion

        #region ActionBehaviourBase

        /// <summary>
        /// Execute this action behaviour
        /// </summary>
        public override void Execute(ActionExecutor executor)
        {
            ShootBullet(executor);
        }

        /// <summary>
        /// Initialize this action
        /// </summary>
        public override void Init(ActionExecutor executor)
        {
            executor.AddFlag(FLAG_SHOOTING, false);

            var timer = new ActionExecutorTimer(ResetShooting, FireRate);
            executor.RegisterTimer(string.Format("{0}{1}", TIMERKEY, this.GetInstanceID()), timer);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Shoot the bullet
        /// </summary>
        protected void ShootBullet(ActionExecutor executor)
        {
            executor.SetFlag(FLAG_SHOOTING, true);
            string name = executor.controller.gameObject.name;

            GameObject projectileObject = PoolerManager.Instance.projectilePooler.GetObject();
            if(projectileObject != null)
            {
                InitializeProjectile(executor, projectileObject);
            }

            executor.StartTimer(string.Format("{0}{1}",TIMERKEY,this.GetInstanceID()));
        }

        protected void InitializeProjectile(ActionExecutor executor, GameObject projectile)
        {
            ProjectileController projectileController = projectile.GetComponent<ProjectileController>();
            SpriteRenderer projectileRenderer = projectile.GetComponent<SpriteRenderer>();
            Collider2D collider = projectile.GetComponent<Collider2D>();

            projectile.transform.position = executor.projectileOrigin.position;
            projectileController.SetDirection(new Vector2(executor.metaData.PhysicsData.FacingDirection.x, 0f));

            projectileRenderer.sprite = projectileInfo.sprite;
            ResizeColliderToSprite(projectileRenderer.sprite, collider);

            projectileController.metaData.MovementData.MovementSpeed = MoveSpeed;
            projectileController.CurrentActionSpec = projectileInfo;
            projectileController.Shooter = executor.controller;

            projectile.SetActive(true);
        }

        protected void ResizeColliderToSprite(Sprite sprite, Collider2D collider)
        {
            collider.offset = Vector2.zero;
            
            if(collider is CircleCollider2D)
            {
                float spriteRadius = sprite.bounds.extents.x;
                var colliderCircle = (CircleCollider2D)collider;
                colliderCircle.radius = spriteRadius;
            }
        }

        /// <summary>
        /// Reset the shooting state
        /// </summary>
        protected void ResetShooting(ActionExecutor executor)
        {
            executor.SetFlag(FLAG_SHOOTING, false);
        }

        #endregion
    }
}
