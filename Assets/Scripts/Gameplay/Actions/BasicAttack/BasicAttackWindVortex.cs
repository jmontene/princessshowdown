﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    /// <summary>
    /// Water form's basic attack
    /// Manifest 3 ice shards in front of Rose
    /// </summary>
    [CreateAssetMenu(fileName = "BasicAttack_WindVortex", menuName = "Tricolor/Actions/BasicAttack/WindVortex", order = 3)]
    public class BasicAttackWindVortex : ActionBehaviourBase
    {
        public ProjectileActionSpec windSpec;

        public override void Execute(ActionExecutor executor)
        {
            GameObject wind = PoolerManager.Instance.windVortexPooler.GetObject();
            if(wind == null)
            {
                return;
            }

            ProjectileController controller = wind.GetComponent<ProjectileController>();
            controller.Shooter = executor.controller;
            controller.CurrentActionSpec = windSpec;
            wind.GetComponent<ActorMetaData>().MovementData.SetDirection(Vector2.right * Mathf.Sign(executor.gameObject.transform.localScale.x));

            Transform bones = executor.gameObject.transform.Find("Bones");
            Transform ik = bones.Find("IK");
            Transform rArm = ik.Find("Right Arm IK");
            Transform spellOrigin = rArm.Find("SpellOrigin");
            wind.transform.position = spellOrigin.position;

            float xScale = 1f;
            if(Mathf.Sign(executor.controller.metaData.PhysicsData.FacingDirection.x) !=
                Mathf.Sign(wind.transform.localScale.x))
            {
                xScale = -1f;
            }
            wind.transform.localScale = new Vector2(xScale * wind.transform.localScale.x, wind.transform.localScale.y);
            wind.SetActive(true);

            AudioManager.Instance.PlaySFX(windSpec.shotSound, 4f);
        }

        public override void Init(ActionExecutor executor)
        {
            
        }
    }
}
