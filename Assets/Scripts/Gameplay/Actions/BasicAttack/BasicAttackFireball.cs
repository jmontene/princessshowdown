﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    [CreateAssetMenu(fileName = "BasicAttack_Fireball", menuName = "Tricolor/Actions/BasicAttack/Fireball", order = 3)]
    public class BasicAttackFireball : ActionBehaviourBase
    {
        public ProjectileActionSpec fireballSpec;

        public override void Execute(ActionExecutor executor)
        {
            GameObject fireball = PoolerManager.Instance.projectilePooler.GetObject();
            if (fireball == null) return;

            ProjectileController controller = fireball.GetComponent<ProjectileController>();
            controller.Shooter = executor.controller;
            controller.CurrentActionSpec = fireballSpec;
            fireball.GetComponent<ActorMetaData>().MovementData.SetDirection(executor.controller.metaData.PhysicsData.FacingDirection);

            fireball.transform.position = executor.projectileOrigin.position;

            float xScale = 1f;
            if (Mathf.Sign(executor.controller.metaData.PhysicsData.FacingDirection.x) !=
                Mathf.Sign(fireball.transform.localScale.x))
            {
                xScale = -1f;
            }
            fireball.transform.localScale = new Vector2(xScale * fireball.transform.localScale.x, fireball.transform.localScale.y);
            fireball.SetActive(true);

            AudioManager.Instance.PlaySFX(fireballSpec.shotSound, 1f);
        }

        public override void Init(ActionExecutor executor)
        {

        }
    }
}
