﻿using Tricolor.Engine;
using UnityEngine;
using DG.Tweening;

namespace Tricolor.Gameplay
{
    [CreateAssetMenu(fileName = "BasicAttack_Dive", menuName = "Tricolor/Actions/BasicAttack/Dive", order = 4)]
    public class BasicAttackDive : ActionBehaviourBase
    {
        public float jumpHeight;
        public float timeToTop;
        public float timeAtTop;
        public float timeToLand;

        public override void Execute(ActionExecutor executor)
        {
            executor.controller.LockActor();
            var ai = executor.enemyAi;
            Vector3 topPosition = ai.target.position + Vector3.up * jumpHeight;

            executor.gameObject.transform.DOMove(topPosition, timeToTop);
            executor.StartTimer("Diver_Dive");
        }

        public override void Init(ActionExecutor executor)
        {
            executor.enemyAi.target = GameObject.Find("Rose").transform;

            var stage2Timer = new ActionExecutorTimer(Stage2, timeToTop);
            executor.RegisterTimer("Diver_Dive", stage2Timer);
        }

        protected void Stage2(ActionExecutor executor)
        {
            executor.controller.SetAnimationTrigger("Dive");
        }
    }
}
