﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    /// <summary>
    /// Water form's basic attack
    /// Manifest 3 ice shards in front of Rose
    /// </summary>
    [CreateAssetMenu(fileName = "BasicAttack_IceShard", menuName = "Tricolor/Actions/BasicAttack/IceShards", order = 2)]
    public class BasicAttackIceShard : ActionBehaviourBase
    {
        public ProjectileActionSpec shardSpec;

        public override void Execute(ActionExecutor executor)
        {
            GameObject shard = PoolerManager.Instance.iceShardPooler.GetObject();
            if(shard == null)
            {
                return;
            }

            ProjectileController controller = shard.GetComponent<ProjectileController>();
            controller.Shooter = executor.controller;
            controller.CurrentActionSpec = shardSpec;
            shard.GetComponent<ActorMetaData>().MovementData.SetDirection(Vector2.right * Mathf.Sign(executor.gameObject.transform.localScale.x));

            Transform bones = executor.gameObject.transform.Find("Bones");
            Transform ik = bones.Find("IK");
            Transform rArm = ik.Find("Right Arm IK");
            Transform spellOrigin = rArm.Find("SpellOrigin");
            shard.transform.position = spellOrigin.position;

            float xScale = 1f;
            if(Mathf.Sign(executor.controller.metaData.PhysicsData.FacingDirection.x) !=
                Mathf.Sign(shard.transform.localScale.x))
            {
                xScale = -1f;
            }
            shard.transform.localScale = new Vector2(xScale * shard.transform.localScale.x, shard.transform.localScale.y);
            shard.SetActive(true);

            AudioManager.Instance.PlaySFX(shardSpec.shotSound, 1f);
        }

        public override void Init(ActionExecutor executor)
        {
            
        }
    }
}
