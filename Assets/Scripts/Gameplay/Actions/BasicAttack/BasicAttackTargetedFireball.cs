﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    [CreateAssetMenu(fileName = "BasicAttack_TargetedFireball", menuName = "Tricolor/Actions/BasicAttack/TargetedFireball", order = 4)]
    public class BasicAttackTargetedFireball : ActionBehaviourBase
    {
        public ProjectileActionSpec fireballSpec;

        public override void Execute(ActionExecutor executor)
        {
            GameObject fireball = PoolerManager.Instance.projectilePooler.GetObject();
            if (fireball == null) return;

            ProjectileController controller = fireball.GetComponent<ProjectileController>();
            controller.Shooter = executor.controller;
            controller.CurrentActionSpec = fireballSpec;
            controller.metaData.MovementData.MovementSpeed = fireballSpec.moveSpeed;

            Vector2 direction = (executor.target.position - executor.gameObject.transform.position).normalized;
            Debug.Log(direction);
            fireball.GetComponent<ActorMetaData>().MovementData.SetDirection(direction);

            fireball.transform.position = executor.projectileOrigin.position;
            fireball.SetActive(true);

            AudioManager.Instance.PlaySFX(fireballSpec.shotSound, 1f);
        }

        public override void Init(ActionExecutor executor)
        {
            executor.target = GameObject.Find("Rose").transform;
        }
    }
}
