﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    [CreateAssetMenu(fileName = "WindVortexActionSpec", menuName = "Tricolor/Actions/Projectile/WindVortex", order = 3)]
    public class WindVortexActionSpec : ProjectileActionSpec
    {
        public override void OnHit(ProjectileController thisProjectile, ActorController shooter, ActorController receiver)
        {
            if (thisProjectile.hitObjects.Contains(receiver.gameObject)) return;

            thisProjectile.hitObjects.Add(receiver.gameObject);
            float attackValue = shooter.GetStat(ActorStrings.STAT_ATTACK) + baseDamage;
            receiver.DealDamage(this, new DealDamageEventArgs(attackValue));
        }
    }
}
