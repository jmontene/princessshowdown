﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    [CreateAssetMenu(fileName = "IceShardActionSpec", menuName = "Tricolor/Actions/Projectile/IceShard", order = 2)]
    public class IceShardActionSpec : ProjectileActionSpec
    {
        public override void OnHit(ProjectileController thisProjectile, ActorController shooter, ActorController receiver)
        {
            if (thisProjectile.hitObjects.Contains(receiver.gameObject)) return;

            thisProjectile.hitObjects.Add(receiver.gameObject);
            float attackValue = shooter.GetStat(ActorStrings.STAT_ATTACK) + baseDamage;
            receiver.DealDamage(this, new DealDamageEventArgs(attackValue));
        }
    }
}
