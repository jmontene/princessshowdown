﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class FirePillar : EntityBehaviourBase
    {
        public Collider2D triggerCollider;
        public Animator pillarAnimator;
        public float pillarTimeToLive = 0.5f;
        public AudioClip entranceClip;

        protected Timer pillarTimer;

        protected override void Start()
        {
            base.Start();
            pillarTimer = new Timer(DeactivatePillar, pillarTimeToLive);
        }

        public void EnableCollider()
        {
            triggerCollider.enabled = true;
        }

        public void DisableCollider()
        {
            triggerCollider.enabled = false;
        }

        public void ActivatePillar()
        {
            pillarAnimator.SetTrigger("Enter");
            AudioManager.Instance.PlaySFX(entranceClip, 1f);
        }

        public void SetOffDeactivateTimer()
        {
            pillarTimer.Start();
        }

        public void DeactivatePillar()
        {
            pillarAnimator.SetTrigger("Exit");
        }
    }
}
