﻿using UnityEngine;

namespace Tricolor.Gameplay
{
    public class Shield : MonoBehaviour
    {
        public void DisableShield()
        {
            gameObject.SetActive(false);
        }
    }
}
