﻿using UnityEngine;
using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class Refresher : MonoBehaviour
    {
        public Animator anim;
        public AudioClip refreshSound;

        protected bool inDash = false;

        public void Refreshed()
        {
            if (inDash) return;
            inDash = true;
            anim.SetTrigger("Dashed");
            AudioManager.Instance.PlaySFX(refreshSound, 0.5f);
        }

        public void ResetRefresherAnimation()
        {
            inDash = false;
        }
    }
}
