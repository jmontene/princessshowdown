﻿using Tricolor.Engine;
using Cinemachine;
using UnityEngine;

namespace Tricolor.Gameplay
{

    public enum CamSettingsType
    {
        CAMERA_Y,
        SWITCH_FOLLOW_CAM
    }

    public class ChangeVCamSettingsTrigger : TriggerCommandBase
    {
        public CinemachineVirtualCamera targetCam;
        public CamSettingsType settingType;
        public float value;
        public Transform target;

        public override void ExecuteTrigger()
        {
            switch (settingType)
            {
                case CamSettingsType.CAMERA_Y:
                    var transposer = targetCam.GetCinemachineComponent<CinemachineFramingTransposer>();
                    transposer.m_ScreenY = value;
                    break;
                case CamSettingsType.SWITCH_FOLLOW_CAM:
                    targetCam.Follow = target;
                    targetCam.MoveToTopOfPrioritySubqueue();
                    break;
            }
        }
    }
}
