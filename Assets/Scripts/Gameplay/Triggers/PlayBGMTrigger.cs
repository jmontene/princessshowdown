﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class PlayBGMTrigger : TriggerCommandBase
    {
        public AudioClip bgm;

        public override void ExecuteTrigger()
        {
            base.ExecuteTrigger();
            AudioManager.Instance.PlayBGM(bgm);
        }
    }
}
