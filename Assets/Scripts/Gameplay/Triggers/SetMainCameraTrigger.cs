﻿using Tricolor.Engine;
using Cinemachine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class SetMainCameraTrigger : TriggerCommandBase
    {
        public CinemachineVirtualCamera targetCamera;

        public override void ExecuteTrigger()
        {
            targetCamera.MoveToTopOfPrioritySubqueue();
            GameObject.Find("Rose").GetComponent<PlayerMetaData>().currentCam = targetCamera;
        }
    }
}
