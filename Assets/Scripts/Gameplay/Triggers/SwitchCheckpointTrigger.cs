﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class SwitchCheckpointTrigger : TriggerCommandBase
    {
        public Checkpoint checkpoint;
        public PlayerController target;

        public override void ExecuteTrigger()
        {
            target.playerMetaData.currentCheckpoint = checkpoint;
        }
    }
}
