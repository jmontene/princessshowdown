﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class BulletWaveIntervalTrigger : TriggerCommandBase
    {
        public float interval = 3f;
        public float delay = 0f;
        public bool stopInterval = false;

        public override void ExecuteTrigger()
        {
            if (stopInterval)
            {
                BulletWaveManager.Instance.StopInterval();
            }
            else
            {
                BulletWaveManager.Instance.SetInterval(interval);
                BulletWaveManager.Instance.SetDelay(delay);
                BulletWaveManager.Instance.ShootWaveInterval();
            }
        }
    }
}
