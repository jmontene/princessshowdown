﻿using Tricolor.Engine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine;
using RPGTALK.Timeline;

namespace Tricolor.Gameplay
{
    public class StartTimelineTrigger : TriggerCommandBase
    {
        public PlayableDirector director;
        public RPGTalkTimeline rpgTimeline;

        public override void ExecuteTrigger()
        {
            rpgTimeline.timelineDirector = director;
            director.Play();
        }
    }
}
