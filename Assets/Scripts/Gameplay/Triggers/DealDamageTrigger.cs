﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class DealDamageTrigger : TriggerCommandBase
    {
        public ActorController controller;
        public float damageToDeal;

        protected DealDamageEventArgs args;

        protected void Awake()
        {
            args = new DealDamageEventArgs(damageToDeal);
        }

        public override void ExecuteTrigger()
        {
            controller.DealDamage(this, args);
            GetComponent<Collider2D>().enabled = false;
        }
    }
}
