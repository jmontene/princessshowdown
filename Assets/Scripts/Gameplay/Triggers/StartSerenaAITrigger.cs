﻿using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class StartSerenaAITrigger : TriggerCommandBase
    {
        public SerenaAI ai;

        public override void ExecuteTrigger()
        {
            base.ExecuteTrigger();
            ai.StartAI();
        }
    }
}
