﻿using Tricolor.Engine;

namespace Tricolor.Gameplay
{
    public class TurnOffMusicTrigger : TriggerCommandBase
    {
        public float fadeDuration = 2f;

        public override void ExecuteTrigger()
        {
            AudioManager.Instance.FadeOut(fadeDuration);
        }
    }
}
