﻿using Tricolor.Engine;
using UnityEngine;

namespace Tricolor.Gameplay
{
    public class DiverEnemyAI : BasicEnemyAI
    {
        [Header("Diver")]
        public float diveSpeed;
        public Collider2D damagingCollider;

        protected bool diving = false;
        protected DealDamageEventArgs dealDamageArgs;

        protected override void Start()
        {
            base.Start();
            dealDamageArgs = new DealDamageEventArgs(2f);
            actorController.CollideEvent += HandleCollide;
        }

        public void Dive()
        {
            diving = true;
            actorController.metaData.PhysicsData.Velocity = Vector2.down * diveSpeed;
        }

        public void FinishDive()
        {
            actorController.UnlockActor();
            diving = false;
        }

        public override void EntityUpdate()
        {
            if (diving) return;
            base.EntityUpdate();
        }

        protected void Collide(ActorController other)
        {
            if (other == null) return;
            if (!damagingCollider.enabled) return;
            if (!other.CompareTag("Player")) return;
            damagingCollider.enabled = false;
            other.DealDamage(this, dealDamageArgs);
        }

        protected void HandleCollide(object sender, CollisionEventArgs args)
        {
            Collide(args.Other);
        }
    }
}
