﻿using Tricolor.Engine;
using UnityEngine;
using System;

namespace Tricolor.Gameplay
{
    public class BasicEnemyAI : EntityBehaviourBase
    {
        public bool enablePatrol = true;
        public float patrolDistance = 1f;
        public float minIdleTime = 1f;
        public float maxIdleTime = 3f;
        public float detectionRadius = 1f;

        [HideInInspector]
        public Transform target;

        protected ActorController actorController;
        protected Timer idleTimer;
        protected int moveDirection;
        protected Transform detectTarget;
        protected EventArgs emptyEventArgs;
        protected bool moveRight;
        protected float originalX;
        protected bool idling;

        protected override void Awake()
        {
            base.Awake();
            actorController = GetComponent<ActorController>();
            detectTarget = GameObject.Find("Rose").transform;
            emptyEventArgs = new EventArgs();
            moveRight = true;
            moveDirection = 1;
            originalX = transform.position.x;
            idling = false;
        }

        protected override void Start()
        {
            base.Start();

            idleTimer = new Timer(ResumePatrol, UnityEngine.Random.Range(minIdleTime, maxIdleTime));
            UpdateFacing();
        }

        public override void EntityUpdate()
        {
            base.EntityUpdate();

            float curDistance = Vector3.Distance(transform.position, detectTarget.position);
            if (curDistance <= detectionRadius)
            {
                AttackPlayer();
                UpdateFacing();
                return;
            }

            if (!enablePatrol)
            {
                UpdateFacing();
                return;
            }

            Patrol();
            UpdateFacing();
        }

        protected void UpdateFacing()
        {
            Vector2 fDir = actorController.metaData.PhysicsData.FacingDirection;
            Vector3 lScale = transform.localScale;
            if (Mathf.Sign(fDir.x) != -Mathf.Sign(lScale.x))
            {
                lScale.x = -lScale.x;
                transform.localScale = lScale;
            }
        }

        protected void Patrol()
        {
            actorController.metaData.MovementData.SetDirection(Vector2.right * moveDirection);
            if(moveDirection != 0) actorController.metaData.PhysicsData.FacingDirection = Vector2.right * moveDirection;
            if((moveRight && transform.position.x >= originalX + patrolDistance) ||
               (!moveRight && transform.position.x <= originalX))
            {
                if(!idling) BeIdle();
            }
        }

        protected void ResumePatrol()
        {
            idling = false;
            moveDirection = (moveRight ? -1 : 1);
            gameObject.transform.localScale = new Vector3(-gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
            moveRight = !moveRight;
        }

        protected void BeIdle()
        {
            idling = true;
            moveDirection = 0;
            idleTimer.SetDuration(UnityEngine.Random.Range(minIdleTime, maxIdleTime));
            idleTimer.Start();
        }

        protected virtual void AttackPlayer()
        {
            actorController.metaData.MovementData.SetDirection(Vector2.zero);
            Vector2 facingDir = actorController.metaData.PhysicsData.FacingDirection;
            float diff = detectTarget.position.x - transform.position.x;
            if((facingDir.x >= 0f && diff < 0f) ||
                (facingDir.x < 0f && diff >= 0f))
            {
                facingDir.x = -facingDir.x;
                actorController.metaData.PhysicsData.FacingDirection = facingDir;
                UpdateFacing();
            }
            actorController.Attack(this, emptyEventArgs);
        }
    }
}
