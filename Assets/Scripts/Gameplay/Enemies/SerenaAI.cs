﻿using Tricolor.Engine;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using RPGTALK.Timeline;

namespace Tricolor.Gameplay
{
    public class SerenaAI : EntityBehaviourBase
    {
        public Transform target;

        [Header("Waypoint Movement")]
        public List<Transform> waypoints;
        public float startingWaypointReachTime = 2f;
        public float minIdleTime = 0.5f;
        public float maxIdleTime = 1f;

        [Header("Attacking")]
        public float startingSpecialAttackCooldown = 4f;

        [Header("Fire Pillars")]
        public GameObject firePillarParent;
        public float timeBetweenPillars = 0.5f;

        [Header("Floor is now Lava")]
        public GameObject lavaFloor;
        public float lavaFloorDuration = 5f;

        [Header("Lifetime")]
        public List<GameObject> onRoseReviveTriggers;
        public List<GameObject> onRoseReviveDisableTriggers;
        public PlayableDirector endingDirector;
        public RPGTalkTimeline rpgTalkTimeline;

        [Header("Voices")]
        public AudioClip bulletWaveVoice;
        public AudioClip firePillarVoice;
        public AudioClip floorLavaVoice;
        public List<AudioClip> hurtVoices;


        protected ActorController actorController;
        protected EventArgs eventArgs;
        protected bool started;
  
        protected Timer waypointTimer;
        protected Timer idleTimer;
        protected Timer specialTimer;

        protected Transform currentWaypoint;
        protected bool movingToWaypoint = false;
        protected bool hasGoneBelowHalf = false;
        protected float wayPointReachTime;

        protected List<string> specialAttacks;
        protected float specialCooldown;
        protected List<FirePillar> firePillars;

        protected Vector3 origPosition;
        protected AudioSource audioSource;


        protected override void Awake()
        {
            base.Awake();

            actorController = GetComponent<ActorController>();
            eventArgs = new EventArgs();
            started = false;

            currentWaypoint = waypoints[UnityEngine.Random.Range(0, waypoints.Count)];
            waypoints.Remove(currentWaypoint);
            wayPointReachTime = startingWaypointReachTime;

            specialAttacks = new List<string>();
            specialAttacks.Add("BulletWave");
            specialAttacks.Add("FirePillars");

            specialCooldown = startingSpecialAttackCooldown;

            firePillars = new List<FirePillar>(firePillarParent.GetComponentsInChildren<FirePillar>());
            origPosition = transform.position;

            audioSource = GetComponent<AudioSource>();
        }

        protected override void Start()
        {
            base.Start();

            waypointTimer = new Timer(StartIdling, wayPointReachTime);
            idleTimer = new Timer(FinishIdling, minIdleTime);
            specialTimer = new Timer(DoSpecialAttack, specialCooldown);
            actorController.metaData.PhysicsData.FacingDirection = Vector2.left;

            actorController.DieEvent += HandleDie;
        }

        public override void EntityUpdate()
        {
            base.EntityUpdate();
            if (!started) return;

            UpdateFacing();
            actorController.Attack(this, eventArgs);
            if(!hasGoneBelowHalf && actorController.GetStat(ActorStrings.STAT_HP) <= actorController.GetBaseStat(ActorStrings.STAT_HP) / 2f)
            {
                hasGoneBelowHalf = true;
                specialAttacks.Add("FloorLava");
            }
        }

        public void StartAI()
        {
            started = true;

            PlayerController roseController = target.GetComponent<PlayerController>();
            roseController.DieEvent += OnRoseDead;
            roseController.ReviveEvent += OnRoseRevive;

            specialTimer.Start();
            StartMovementToWaypoint();
        }

        public void PlayHurtSound()
        {
            AudioClip clip = hurtVoices[UnityEngine.Random.Range(0, hurtVoices.Count)];
            audioSource.PlayOneShot(clip);
        }

        protected void SetNewWaypoint()
        {
            Transform temp = currentWaypoint;
            currentWaypoint = waypoints[UnityEngine.Random.Range(0, waypoints.Count)];
            waypoints.Remove(currentWaypoint);
            waypoints.Add(temp);
        }

        protected void StartMovementToWaypoint()
        {
            movingToWaypoint = true;
            transform.DOMove(currentWaypoint.position, wayPointReachTime);
            waypointTimer.SetDuration(wayPointReachTime);
            waypointTimer.Start();
        }

        protected void StartIdling()
        {
            movingToWaypoint = false;
            idleTimer.SetDuration(UnityEngine.Random.Range(minIdleTime, maxIdleTime));
            idleTimer.Start();
        }

        protected void FinishIdling()
        {
            SetNewWaypoint();
            StartMovementToWaypoint();
        }

        protected void DoSpecialAttack()
        {
            if (!started) return;
            string attack = specialAttacks[UnityEngine.Random.Range(0, specialAttacks.Count)];
            switch (attack)
            {
                case "BulletWave":
                    StartCoroutine(BulletWaveAttack());
                    break;
                case "FirePillars":
                    StartCoroutine(FirePillarAttack());
                    break;
                case "FloorLava":
                    StartCoroutine(TheFloorIsLavaAttack());
                    break;
            }   
        }

        protected IEnumerator BulletWaveAttack()
        {
            audioSource.PlayOneShot(bulletWaveVoice);
            yield return new WaitForSeconds(0.5f);
            BulletWaveManager.Instance.ShootWave();
            specialTimer.Start();
        }

        protected IEnumerator FirePillarAttack()
        {
            audioSource.PlayOneShot(firePillarVoice);
            yield return new WaitForSeconds(0.5f);
            foreach (FirePillar pillar in firePillars)
            {
                if (!started) break;
                pillar.ActivatePillar();
                yield return new WaitForSeconds(timeBetweenPillars);
            }
            specialTimer.Start();
        }

        protected IEnumerator TheFloorIsLavaAttack()
        {
            audioSource.PlayOneShot(floorLavaVoice, 3f);
            yield return new WaitForSeconds(floorLavaVoice.length);
            lavaFloor.SetActive(false);
            yield return new WaitForSeconds(lavaFloorDuration);
            lavaFloor.SetActive(true);
            specialTimer.Start();
        }

        protected void UpdateFacing()
        {
            Vector2 fDir = actorController.metaData.PhysicsData.FacingDirection;
            if((target.position.x < transform.position.x && fDir.x >= 0) ||
                (target.position.x >= transform.position.x && fDir.x < 0))
            {
                Flip();
            }
        }

        protected void Flip()
        {
            Vector3 lScale = transform.localScale;
            lScale.x = -lScale.x;
            transform.localScale = lScale;

            Vector2 fDir = actorController.metaData.PhysicsData.FacingDirection;
            fDir.x = -fDir.x;
            actorController.metaData.PhysicsData.FacingDirection = fDir;
        }

        protected void StopAI()
        {
            PlayerController roseController = target.GetComponent<PlayerController>();
            roseController.DieEvent -= OnRoseDead;
            lavaFloor.SetActive(true);
            hasGoneBelowHalf = false;

            started = false;
            waypointTimer.Stop();
            specialTimer.Stop();
            idleTimer.Stop();
            AudioManager.Instance.FadeOut(2f);
        }

        protected void OnRoseDead(object _sender, EventArgs _args)
        {
            StopAI();
        }

        protected void OnRoseRevive(object _sender, EventArgs _args)
        {
            transform.position = origPosition;
            actorController.metaData.ActorStats.ResetStat(ActorStrings.STAT_HP);
            foreach(GameObject obj in onRoseReviveTriggers)
            {
                obj.SetActive(true);
            }
            foreach (GameObject obj in onRoseReviveDisableTriggers)
            {
                obj.SetActive(false);
            }

            PlayerController roseController = target.GetComponent<PlayerController>();
            roseController.ReviveEvent -= OnRoseRevive;
        }

        protected void HandleDie(object _sender, EventArgs _args)
        {
            target.GetComponent<PlayerStateBehaviour>().gameOver = true;
            lavaFloor.SetActive(true);

            StopAI();
            rpgTalkTimeline.timelineDirector = endingDirector;
            endingDirector.Play();
        }
    }
}
