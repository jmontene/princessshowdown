﻿using Tricolor.Engine;
using System;

namespace Tricolor.Gameplay
{
    public class DebugEnemyAI : EntityBehaviourBase
    {
        protected ActorController actorController;
        protected EventArgs emptyEventArgs;

        protected override void Awake()
        {
            base.Awake();
            actorController = GetComponent<ActorController>();
            emptyEventArgs = new EventArgs();
        }

        public override void EntityUpdate()
        {
            base.EntityUpdate();
            actorController.Attack(this, emptyEventArgs);
        }
    }
}
