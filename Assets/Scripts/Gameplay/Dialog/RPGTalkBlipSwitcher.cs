﻿using UnityEngine;
using RPGTALK;

namespace Tricolor.Gameplay
{
    public class RPGTalkBlipSwitcher : MonoBehaviour
    {
        public AudioClip roseClip;
        public AudioClip serenaClip;
        public RPGTalk dialog;

        protected void Awake()
        {
            dialog.OnChangeDialoger += SwitchClips;
            dialog.OnInitializeDialoger += SetCorrectBlip;
        }

        protected void SwitchClips(string newDialoger)
        {
            SetCorrectBlip();
        }

        protected void SetCorrectBlip()
        {
            if(dialog.dialogerUI.text.StartsWith("Rose"))
            {
                dialog.textAudio = roseClip;
            }else if(dialog.dialogerUI.text.StartsWith("Serena"))
            {
                dialog.textAudio = serenaClip;
            }
        }
    }
}
